package ru.walfi.nurturenook.feature_game_words.domain.use_case

import ru.walfi.nurturenook.core.util.Role
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository

class GetChildIdUseCase(
    private val repository: GameWordsRepository
) {

    suspend operator fun invoke(role: Role): Int{
        return repository.getChildId(role)
    }
}