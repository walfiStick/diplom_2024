package ru.walfi.nurturenook.feature_game_words.presentation.create_lvl

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.core.model.InvalidGameWordLvlException
import ru.walfi.nurturenook.feature_game_words.domain.use_case.AddGameWordsLvlUseCase
import ru.walfi.nurturenook.feature_game_words.domain.use_case.GameWordsUseCases
import ru.walfi.nurturenook.feature_parent_home.presentation.parent_child_list.ParentChildListViewModel
import javax.inject.Inject

@HiltViewModel
class CreateWordLvlViewModel @Inject constructor(
    private val gameWordsUseCases: GameWordsUseCases
): ViewModel() {

    private val _state = mutableStateOf(CreateWordLvlState())
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: CreateWordLvlEvent){
        when(event){
            is CreateWordLvlEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
            is CreateWordLvlEvent.PressedLoadPhotoButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.LaunchGallery)
                }
            }
            is CreateWordLvlEvent.SaveUri -> {
                _state.value = state.value.copy(
                    uri = event.uri
                )
            }
            is CreateWordLvlEvent.FirstAnswerChange ->{
                _state.value = state.value.copy(
                    firstAnswerChoice = event.value
                )
            }
            is CreateWordLvlEvent.SecondAnswerChange ->{
                _state.value = state.value.copy(
                    secondAnswerChoice = event.value
                )
            }
            is CreateWordLvlEvent.ThirdAnswerChange ->{
                _state.value = state.value.copy(
                    thirdAnswerChoice = event.value
                )
            }
            is CreateWordLvlEvent.ChooseAnswer ->{
                _state.value = state.value.copy(
                    answer = event.answer
                )
            }
            is CreateWordLvlEvent.PressedSaveButton -> {
                viewModelScope.launch {
                    try {
                        gameWordsUseCases.addGameWordsLvlUseCase(
                            GameWordsLvl(
                                firstWord = _state.value.firstAnswerChoice,
                                secondWord = _state.value.secondAnswerChoice,
                                thirdWord = _state.value.thirdAnswerChoice,
                                answer = _state.value.answer,
                                picture = _state.value.uri.toString()
                            )
                        )
                        _eventFlow.emit(UiEvent.NavigateUp)
                    }catch (e: InvalidGameWordLvlException){
                        viewModelScope.launch{
                            _eventFlow.emit(UiEvent.ShowSnackbar(e.message ?: "Что-то пошло не так"))
                        }
                    }
                }
            }
        }
    }

    sealed class UiEvent{
        object NavigateUp: UiEvent()
        object LaunchGallery: UiEvent()
        class ShowSnackbar(val message: String) : UiEvent()
    }
}