package ru.walfi.nurturenook.feature_game_words.presentation.create_lvl

import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.IconButton
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.R
import ru.walfi.nurturenook.core.util.BaseButton
import ru.walfi.nurturenook.core.util.BaseLoadButton
import ru.walfi.nurturenook.ui.theme.backBasicTextColor
import ru.walfi.nurturenook.ui.theme.backgroundColor
import ru.walfi.nurturenook.ui.theme.selectedRadioButtonColor
import ru.walfi.nurturenook.ui.theme.unselectedRadioButtonColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateWordLvlScreen(
    navController: NavController,
    viewModel: CreateWordLvlViewModel = hiltViewModel()
) {
    /**/
//    var imageUri by remember { mutableStateOf<Uri?>(null) } // del
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val context = LocalContext.current
    val bitmap = remember { mutableStateOf<Bitmap?>(null) }
    val launcher = rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) { uri: Uri? ->
        viewModel.onEvent(CreateWordLvlEvent.SaveUri(uri))
//        /**/ imageUri = uri//del
    }

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is CreateWordLvlViewModel.UiEvent.NavigateUp -> {
                    navController.navigateUp()
                }
                is CreateWordLvlViewModel.UiEvent.LaunchGallery -> {
                    launcher.launch("image/*")
                }
                is CreateWordLvlViewModel.UiEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(event.message)
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        modifier = Modifier.background(backgroundColor),
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                navigationIcon = {
                    IconButton(onClick = { viewModel.onEvent(CreateWordLvlEvent.PressedBackButton) }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Назад"
                        )
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .systemBarsPadding(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            state.uri?.let {
//            /**/imageUri?.let {//del
            if (Build.VERSION.SDK_INT < 28) {
                bitmap.value = MediaStore.Images.Media.getBitmap(context.contentResolver, it)
            } else {
                val source = ImageDecoder.createSource(context.contentResolver, it)
                bitmap.value = ImageDecoder.decodeBitmap(source)
            }
        }
            bitmap.value.let {
                if (it == null) {
                    Image(
                        painter = painterResource(R.drawable.empty_photo),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .height(200.dp)
                            .width(250.dp)
                    )
                } else {
                    Image(
                        bitmap = it.asImageBitmap(),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .height(200.dp)
                            .width(250.dp)
                            .aspectRatio(1f)
                    )
                }
            }
            Spacer(modifier = Modifier.height(24.dp))
            BaseLoadButton(
                text = "Загрузить фото",
                onClick = { viewModel.onEvent(CreateWordLvlEvent.PressedLoadPhotoButton) }
            )
            Spacer(modifier = Modifier.height(24.dp))
            AnswerRow(
                isSelected = state.answer == 1,
                value = state.firstAnswerChoice,
                onClick = { viewModel.onEvent(CreateWordLvlEvent.ChooseAnswer(1)) },
                onValueChange = { viewModel.onEvent(CreateWordLvlEvent.FirstAnswerChange(it)) }
            )
            Spacer(modifier = Modifier.height(8.dp))
            AnswerRow(
                isSelected = state.answer == 2,
                value = state.secondAnswerChoice,
                onClick = { viewModel.onEvent(CreateWordLvlEvent.ChooseAnswer(2)) },
                onValueChange = { viewModel.onEvent(CreateWordLvlEvent.SecondAnswerChange(it)) }
            )
            Spacer(modifier = Modifier.height(8.dp))
            AnswerRow(
                isSelected = state.answer == 3,
                value = state.thirdAnswerChoice,
                onClick = { viewModel.onEvent(CreateWordLvlEvent.ChooseAnswer(3)) },
                onValueChange = { viewModel.onEvent(CreateWordLvlEvent.ThirdAnswerChange(it)) }
            )
            Spacer(modifier = Modifier.height(20.dp))
            BaseButton(
                text = "Сохранить",
                onClick = { viewModel.onEvent(CreateWordLvlEvent.PressedSaveButton) }
            )
        }
    }
}

@Composable
fun AnswerRow(
    isSelected: Boolean,
    value: String,
    onClick: () -> Unit,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier
){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)
    ) {
        RadioButton(
            selected = isSelected,
            onClick = onClick,
            colors = RadioButtonDefaults.colors(
                selectedColor = selectedRadioButtonColor,
                unselectedColor = unselectedRadioButtonColor
            )
        )
        Spacer(modifier = Modifier.width(12.dp))
        BasicTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier
                .clip(RoundedCornerShape(8.dp))
                .background(backBasicTextColor)
                .padding(6.dp)
        )
    }
}
