package ru.walfi.nurturenook.feature_game_words.domain.use_case

data class GameWordsUseCases(
    val addGameWordsLvlUseCase: AddGameWordsLvlUseCase,
    val getWordLvls: GetWordLvls,
    val getGameWordsPurposeUseCase: GetGameWordsPurposeUseCase,
    val getChildIdUseCase: GetChildIdUseCase,
    val updatePurposeUseCase: UpdatePurposeUseCase
)
