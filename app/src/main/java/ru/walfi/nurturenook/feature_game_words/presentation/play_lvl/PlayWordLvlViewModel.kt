package ru.walfi.nurturenook.feature_game_words.presentation.play_lvl

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.Role
import ru.walfi.nurturenook.feature_game_words.domain.use_case.GameWordsUseCases
import javax.inject.Inject

@HiltViewModel
class PlayWordLvlViewModel @Inject constructor(
    private val gameWordsUseCases: GameWordsUseCases
) : ViewModel() {

    private val _state = mutableStateOf(PlayWordLvlState())
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var childId: Int = -1

    init {
        viewModelScope.launch {
            childId = gameWordsUseCases.getChildIdUseCase(Role.Child)
            gameWordsUseCases.getWordLvls().onEach { lvlList ->
                _state.value = state.value.copy(
                    lvlInfoList = lvlList
                )
            }.launchIn(viewModelScope)
        }
    }

    fun onEvent(event: PlayWordLvlEvent) {
        when (event) {
            is PlayWordLvlEvent.ChosenAnswer -> {
                _state.value = state.value.copy(
                    selectedAnswer = event.answer
                )
            }
            is PlayWordLvlEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
            is PlayWordLvlEvent.PressedAnswerButton -> {
                viewModelScope.launch {
                    if (_state.value.lvlInfoList[_state.value.currentLvl].answer == _state.value.selectedAnswer) {
                        _eventFlow.emit(UiEvent.ShowSnackbar("Ответ верный!"))
                        val currentPurpose = gameWordsUseCases.getGameWordsPurposeUseCase(childId = childId).first()
                        gameWordsUseCases.updatePurposeUseCase(
                            currentPurpose.copy(completedValue = currentPurpose.completedValue + 1)
                        )
                        delay(5000)
                        _state.value = state.value.copy(
                            currentLvl = _state.value.currentLvl + 1,
                            selectedAnswer = null
                        )
                    } else if (_state.value.selectedAnswer == null) {
                        _eventFlow.emit(UiEvent.ShowSnackbar("Ответ не выбран"))
                    } else {
                        _eventFlow.emit(UiEvent.ShowSnackbar("Ответ не верный!"))
                    }
                }
            }
        }
    }

    sealed class UiEvent {
        object NavigateUp : UiEvent()
        class ShowSnackbar(val message: String) : UiEvent()
    }
}