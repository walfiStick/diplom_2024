package ru.walfi.nurturenook.feature_game_words.data.data_source

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.core.model.Purpose

const val GAME_WORDS_NAME = "Изучение слов"

@Dao
interface GameWordsDao {

    @Insert
    suspend fun insertGameWordsLvl(gameWordsLvl: GameWordsLvl)

    @Query("SELECT * FROM gamewordslvl")
    fun getWordLvls(): Flow<List<GameWordsLvl>>

    @Query("SELECT * FROM purpose WHERE childId = :childId AND gameName = :gameName")
    fun getGameWordsPurpose(childId: Int, gameName: String): Flow<Purpose>

    @Update
    suspend fun updatePurpose(purpose: Purpose)
}