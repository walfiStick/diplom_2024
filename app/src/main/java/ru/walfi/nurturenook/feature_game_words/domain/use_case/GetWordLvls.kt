package ru.walfi.nurturenook.feature_game_words.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository

class GetWordLvls(
    private val repository: GameWordsRepository
) {

    operator fun invoke(): Flow<List<GameWordsLvl>>{
        return repository.getWordLvls()
    }
}