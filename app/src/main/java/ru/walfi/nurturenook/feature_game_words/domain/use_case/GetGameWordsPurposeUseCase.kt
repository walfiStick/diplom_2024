package ru.walfi.nurturenook.feature_game_words.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository

class GetGameWordsPurposeUseCase(
    private val repository: GameWordsRepository
) {

    operator fun invoke(childId: Int): Flow<Purpose>{
        return repository.getGameWordsPurpose(childId)
    }
}