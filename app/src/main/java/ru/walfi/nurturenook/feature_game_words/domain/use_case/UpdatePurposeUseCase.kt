package ru.walfi.nurturenook.feature_game_words.domain.use_case

import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository

class UpdatePurposeUseCase(
    private val repository: GameWordsRepository
) {

    suspend operator fun invoke(purpose: Purpose){
        repository.updatePurpose(purpose)
    }
}