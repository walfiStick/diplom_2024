package ru.walfi.nurturenook.feature_game_words.presentation.play_lvl

sealed class PlayWordLvlEvent {

    class ChosenAnswer(val answer: Int): PlayWordLvlEvent()

    object PressedAnswerButton: PlayWordLvlEvent()
    object PressedBackButton: PlayWordLvlEvent()
}