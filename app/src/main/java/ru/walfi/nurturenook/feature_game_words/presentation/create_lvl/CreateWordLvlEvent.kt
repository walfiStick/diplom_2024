package ru.walfi.nurturenook.feature_game_words.presentation.create_lvl

import android.net.Uri

sealed class CreateWordLvlEvent{

    object PressedBackButton: CreateWordLvlEvent()
    object PressedSaveButton: CreateWordLvlEvent()
    object PressedLoadPhotoButton: CreateWordLvlEvent()
    class ChooseAnswer(val answer: Int): CreateWordLvlEvent()

    class SaveUri(val uri: Uri?): CreateWordLvlEvent()

    class FirstAnswerChange(val value: String): CreateWordLvlEvent()
    class SecondAnswerChange(val value: String): CreateWordLvlEvent()
    class ThirdAnswerChange(val value: String): CreateWordLvlEvent()
}
