package ru.walfi.nurturenook.feature_game_words.presentation.play_lvl

import ru.walfi.nurturenook.core.model.GameWordsLvl

data class PlayWordLvlState(
    val toolbarText: String = "Изучение слов",
    val lvlInfoList: List<GameWordsLvl> = emptyList(),
    val currentLvl: Int = 0,
    val selectedAnswer: Int? = null
)
