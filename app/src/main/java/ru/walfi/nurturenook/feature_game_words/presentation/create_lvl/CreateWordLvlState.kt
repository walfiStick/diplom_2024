package ru.walfi.nurturenook.feature_game_words.presentation.create_lvl

import android.net.Uri

data class CreateWordLvlState(
    val toolbarText: String = "Создать уровень",
    val uri: Uri? = null,
    val firstAnswerChoice: String = "",
    val secondAnswerChoice: String = "",
    val thirdAnswerChoice: String = "",
    val answer: Int = -1,
)
