package ru.walfi.nurturenook.feature_game_words.presentation.play_lvl

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.material.IconButton
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.R
import ru.walfi.nurturenook.core.util.BaseButton
import ru.walfi.nurturenook.ui.theme.backgroundColor
import ru.walfi.nurturenook.ui.theme.selectedRadioButtonColor
import ru.walfi.nurturenook.ui.theme.unselectedRadioButtonColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PlayWordLvlScreen(
    navController: NavController,
    viewModel: PlayWordLvlViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is PlayWordLvlViewModel.UiEvent.NavigateUp -> {
                    navController.navigateUp()
                }
                is PlayWordLvlViewModel.UiEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(event.message)
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        modifier = Modifier.background(backgroundColor),
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                navigationIcon = {
                    IconButton(onClick = { viewModel.onEvent(PlayWordLvlEvent.PressedBackButton) }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Назад"
                        )
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .systemBarsPadding(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (state.lvlInfoList.isNotEmpty() && state.currentLvl <= state.lvlInfoList.size - 1) {
                Image(
                    painter = painterResource(id = R.drawable.empty_photo),
                    contentDescription = null,
                    modifier = Modifier
                        .size(width = 250.dp, height = 150.dp)
                        .aspectRatio(1f)
                )
                Spacer(modifier = Modifier.height(24.dp))
                Column(
                    horizontalAlignment = Alignment.Start
                ) {
                    AnswerRow(
                        isSelected = state.selectedAnswer == 1,
                        value = state.lvlInfoList[state.currentLvl].firstWord,
                        onClick = { viewModel.onEvent(PlayWordLvlEvent.ChosenAnswer(1)) }
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    AnswerRow(
                        isSelected = state.selectedAnswer == 2,
                        value = state.lvlInfoList[state.currentLvl].secondWord,
                        onClick = { viewModel.onEvent(PlayWordLvlEvent.ChosenAnswer(2)) }
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    AnswerRow(
                        isSelected = state.selectedAnswer == 3,
                        value = state.lvlInfoList[state.currentLvl].thirdWord,
                        onClick = { viewModel.onEvent(PlayWordLvlEvent.ChosenAnswer(3)) }
                    )
                }
                Spacer(modifier = Modifier.height(20.dp))
                BaseButton(
                    text = "Ответить",
                    onClick = { viewModel.onEvent(PlayWordLvlEvent.PressedAnswerButton) }
                )
            }else{
                Text(text = "Заданий нет")
            }
        }
    }
}


@Composable
fun AnswerRow(
    isSelected: Boolean,
    value: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start,
        modifier = modifier
            .padding(horizontal = 20.dp)
    ) {
        RadioButton(
            selected = isSelected,
            onClick = onClick,
            colors = RadioButtonDefaults.colors(
                selectedColor = selectedRadioButtonColor,
                unselectedColor = unselectedRadioButtonColor
            )
        )
        Spacer(modifier = Modifier.width(12.dp))
        Text(text = value)
    }
}