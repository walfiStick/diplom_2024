package ru.walfi.nurturenook.feature_game_words.domain.use_case

import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.core.model.InvalidGameWordLvlException
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository

class AddGameWordsLvlUseCase(
    private val repository: GameWordsRepository
) {
    @Throws(InvalidGameWordLvlException::class)
    suspend operator fun invoke(gameWordsLvl: GameWordsLvl){
        if(gameWordsLvl.firstWord.isBlank()){
            throw InvalidGameWordLvlException("Не все варианты ответов введены")
        }
        if(gameWordsLvl.secondWord.isBlank()){
            throw InvalidGameWordLvlException("Не все варианты ответов введены")
        }
        if(gameWordsLvl.thirdWord.isBlank()){
            throw InvalidGameWordLvlException("Не все варианты ответов введены")
        }
        if(gameWordsLvl.picture == null){
            throw InvalidGameWordLvlException("Картинка не выбрана")
        }
        if(gameWordsLvl.answer == -1){
            throw InvalidGameWordLvlException("Правильный ответ не выбран")
        }
        repository.insertGameWordsLvl(gameWordsLvl)
    }
}