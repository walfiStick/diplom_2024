package ru.walfi.nurturenook.feature_game_words.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.core.util.Role

interface GameWordsRepository {

    suspend fun insertGameWordsLvl(gameWordsLvl: GameWordsLvl)

    fun getWordLvls(): Flow<List<GameWordsLvl>>

    fun getGameWordsPurpose(childId: Int): Flow<Purpose>

    suspend fun getChildId(role: Role): Int

    suspend fun updatePurpose(purpose: Purpose)
}