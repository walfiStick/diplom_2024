package ru.walfi.nurturenook.feature_game_words.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.core.util.Role
import ru.walfi.nurturenook.feature_game_words.data.data_source.GameWordsDao
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository

class GameWordsRepositoryImpl(
    private val dao: GameWordsDao,
    private val dataStoreManager: DataStoreManager
): GameWordsRepository {

    override suspend fun insertGameWordsLvl(gameWordsLvl: GameWordsLvl) {
        dao.insertGameWordsLvl(gameWordsLvl)
    }

    override fun getWordLvls(): Flow<List<GameWordsLvl>> {
        return dao.getWordLvls()
    }

    override fun getGameWordsPurpose(childId: Int): Flow<Purpose> {
        return dao.getGameWordsPurpose(childId, "Изучение слов")
    }

    override suspend fun getChildId(role: Role): Int {
        return if(role == Role.Parent) dataStoreManager.getChildId().first()
        else dataStoreManager.getAccountData().first().accountId
    }

    override suspend fun updatePurpose(purpose: Purpose) {
        dao.updatePurpose(purpose)
    }
}