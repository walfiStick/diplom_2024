package ru.walfi.nurturenook.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.data.data_source.MainDatabase
import ru.walfi.nurturenook.feature_auth.data.repository.AuthRepositoryImpl
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository
import ru.walfi.nurturenook.feature_auth.domain.use_case.AddParentUseCase
import ru.walfi.nurturenook.feature_auth.domain.use_case.AddSpecialistUseCase
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthChildUseCase
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthParentUseCase
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthSpecialistUseCase
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthUseCases
import ru.walfi.nurturenook.feature_auth.domain.use_case.CheckAccountDataUseCase
import ru.walfi.nurturenook.feature_auth.domain.use_case.SaveAccountDataUseCase
import ru.walfi.nurturenook.feature_child_home.data.repository.ChildHomeRepositoryImpl
import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository
import ru.walfi.nurturenook.feature_child_home.domain.use_case.ChangeTaskStatusUseCase
import ru.walfi.nurturenook.feature_child_home.domain.use_case.ChildHomeUseCases
import ru.walfi.nurturenook.feature_child_home.domain.use_case.ChildLogOutUseCase
import ru.walfi.nurturenook.feature_child_home.domain.use_case.GetPurposesUseCase
import ru.walfi.nurturenook.feature_game_words.data.repository.GameWordsRepositoryImpl
import ru.walfi.nurturenook.feature_game_words.domain.repository.GameWordsRepository
import ru.walfi.nurturenook.feature_game_words.domain.use_case.AddGameWordsLvlUseCase
import ru.walfi.nurturenook.feature_game_words.domain.use_case.GameWordsUseCases
import ru.walfi.nurturenook.feature_game_words.domain.use_case.GetGameWordsPurposeUseCase
import ru.walfi.nurturenook.feature_game_words.domain.use_case.GetWordLvls
import ru.walfi.nurturenook.feature_game_words.domain.use_case.UpdatePurposeUseCase
import ru.walfi.nurturenook.feature_parent_child_section.data.repository.ParentChildSectionRepositoryImpl
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.AddTaskUseCase
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.DeleteTaskUseCase
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.GetChildIdParentUseCase
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.GetCommentsUseCase
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.GetSpecialistsUseCase
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.GetTasksUseCase
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.ParentChildSectionUseCases
import ru.walfi.nurturenook.feature_parent_home.data.repository.ParentHomeRepositoryImpl
import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.CheckPresenceChildUseCase
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.ParentHomeUseCases
import ru.walfi.nurturenook.feature_specialist_child_section.data.repository.SpecialistChildSectionRepositoryImpl
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.AddCommentUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.AddPurposeUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.DeleteCommentUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.GetChildIdUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.GetChildUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.GetCommentByChildAndSpecialistIdUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.GetPurposesByChildIdUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.GetSpecialistIdUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.LogOutChildUseCase
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.SpecialistChildSectionUseCases
import ru.walfi.nurturenook.feature_specialist_home.data.repository.SpecialistHomeRepositoryImpl
import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.AddChildUseCase
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.GetAccountIdUseCase as GetSpecialistAccountIdUseCase
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.GetAccountIdUseCase
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.GetChildrenForParentUseCase
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.GetParentUseCase
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.LogOutParentUseCase
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.SaveChildForParentUseCase
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.GetChildrenUseCase
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.GetSpecialistUseCase
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.LogOutUseCase
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.SaveChildIdUseCase
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.SpecialistHomeUseCases
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.CheckPresenceChildUseCase as SpecialistCheckPresenceChildUseCase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideMainDatabase(app: Application): MainDatabase {
        return Room.databaseBuilder(
            app,
            MainDatabase::class.java,
            MainDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideAuthRepository(
        db: MainDatabase,
        dataStoreManager: DataStoreManager
    ): AuthRepository {
        return AuthRepositoryImpl(db.authDao(), dataStoreManager)
    }

    @Provides
    @Singleton
    fun provideSpecialistHomeRepository(
        db: MainDatabase,
        dataStoreManager: DataStoreManager
    ): SpecialistHomeRepository{
        return SpecialistHomeRepositoryImpl(db.specialistHomeDao(), dataStoreManager)
    }

    @Provides
    @Singleton
    fun provideParentHomeRepository(
        db: MainDatabase,
        dataStoreManager: DataStoreManager
    ): ParentHomeRepository{
        return ParentHomeRepositoryImpl(db.parentHomeDao(), dataStoreManager)
    }

    @Provides
    @Singleton
    fun provideSpecialistChildSectionRepository(
        db: MainDatabase,
        dataStoreManager: DataStoreManager
    ): SpecialistChildSectionRepository{
        return SpecialistChildSectionRepositoryImpl(db.specialistChildSectionDao(), dataStoreManager)
    }

    @Provides
    @Singleton
    fun provideParentChildSectionRepository(
        db: MainDatabase,
        dataStoreManager: DataStoreManager
    ): ParentChildSectionRepository{
        return ParentChildSectionRepositoryImpl(db.parentChildSectionDao(), dataStoreManager)
    }

    @Provides
    @Singleton
    fun provideGameWordsRepository(
        dataStoreManager: DataStoreManager,
        db: MainDatabase
    ): GameWordsRepository{
        return GameWordsRepositoryImpl(db.gameWordDao(), dataStoreManager)
    }

    @Provides
    @Singleton
    fun provideChildHomeRepository(
        dataStoreManager: DataStoreManager,
        db: MainDatabase
    ): ChildHomeRepository{
        return ChildHomeRepositoryImpl(dataStoreManager, db.childHomeDao())
    }

    @Provides
    @Singleton
    fun provideAuthUseCases(repository: AuthRepository): AuthUseCases{
        return AuthUseCases(
            addParentUseCase = AddParentUseCase(repository),
            addSpecialistUseCase = AddSpecialistUseCase(repository),
            authSpecialistUseCase = AuthSpecialistUseCase(repository),
            checkAccountDataUseCase = CheckAccountDataUseCase(repository),
            saveAccountDataUseCase = SaveAccountDataUseCase(repository),
            authParentUseCase = AuthParentUseCase(repository),
            authChildUseCase = AuthChildUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideSpecialistHomeUseCases(repository: SpecialistHomeRepository): SpecialistHomeUseCases{
        return SpecialistHomeUseCases(
            getSpecialistUseCase = GetSpecialistUseCase(repository),
            logOutUseCase = LogOutUseCase(repository),
            getChildrenUseCase = GetChildrenUseCase(repository),
            addChildUseCase = AddChildUseCase(repository),
            getAccountIdUseCase = GetSpecialistAccountIdUseCase(repository),
            saveChildIdUseCase = SaveChildIdUseCase(repository),
            checkPresenceChildUseCase = SpecialistCheckPresenceChildUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideParentHomeUseCases(repository: ParentHomeRepository): ParentHomeUseCases{
        return ParentHomeUseCases(
            getAccountIdUseCase = GetAccountIdUseCase(repository),
            getChildrenForParentUseCase = GetChildrenForParentUseCase(repository),
            getParentUseCase = GetParentUseCase(repository),
            logOutParentUseCase = LogOutParentUseCase(repository),
            checkPresenceChildUseCase = CheckPresenceChildUseCase(repository),
            saveChildForParentUseCase = SaveChildForParentUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideSpecialistChildSectionUseCases(repository: SpecialistChildSectionRepository): SpecialistChildSectionUseCases{
        return SpecialistChildSectionUseCases(
            getChildUseCase = GetChildUseCase(repository),
            getChildIdUseCase = GetChildIdUseCase(repository),
            logOutChildUseCase = LogOutChildUseCase(repository),
            addCommentUseCase = AddCommentUseCase(repository),
            getCommentByChildAndSpecialistIdUseCase = GetCommentByChildAndSpecialistIdUseCase(repository),
            getSpecialistIdUseCase = GetSpecialistIdUseCase(repository),
            deleteCommentUseCase = DeleteCommentUseCase(repository),
            addPurposeUseCase = AddPurposeUseCase(repository),
            getPurposesByChildIdUseCase = GetPurposesByChildIdUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideParentChildSectionUseCases(repository: ParentChildSectionRepository): ParentChildSectionUseCases{
        return ParentChildSectionUseCases(
            getCommentsUseCase = GetCommentsUseCase(repository),
            getChildIdParentUseCase = GetChildIdParentUseCase(repository),
            getSpecialistsUseCase = GetSpecialistsUseCase(repository),
            getTasksUseCase = GetTasksUseCase(repository),
            addTaskUseCase = AddTaskUseCase(repository),
            deleteTaskUseCase = DeleteTaskUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideGameWordsUseCases(repository: GameWordsRepository): GameWordsUseCases{
        return GameWordsUseCases(
            addGameWordsLvlUseCase = AddGameWordsLvlUseCase(repository),
            getWordLvls = GetWordLvls(repository),
            getGameWordsPurposeUseCase = GetGameWordsPurposeUseCase(repository),
            getChildIdUseCase = ru.walfi.nurturenook.feature_game_words.domain.use_case.GetChildIdUseCase(repository),
            updatePurposeUseCase = UpdatePurposeUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideChildHomeUseCases(repository: ChildHomeRepository): ChildHomeUseCases{
        return ChildHomeUseCases(
            childLogOutUseCase = ChildLogOutUseCase(repository),
            getChildIdUseCase = ru.walfi.nurturenook.feature_child_home.domain.use_case.GetChildIdUseCase(repository),
            getTasksUseCase = ru.walfi.nurturenook.feature_child_home.domain.use_case.GetTasksUseCase(repository),
            changeTaskStatusUseCase = ChangeTaskStatusUseCase(repository),
            getPurposesUseCase = GetPurposesUseCase(repository)
        )
    }

    @Provides
    @Singleton
    fun provideDataStoreManager(app: Application): DataStoreManager{
        return DataStoreManager(app.applicationContext)
    }
}