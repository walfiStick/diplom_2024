package ru.walfi.nurturenook.feature_specialist_home.presentation.home

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Badge
import androidx.compose.material.BadgedBox
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_child_list.SpecialistChildListScreen
import ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_profile.SpecialistProfileScreen
import ru.walfi.nurturenook.ui.theme.backgroundColor

@Composable
fun SpecialistHomeScreen(
    navController: NavController,
    specialistId: Int?,
    viewModel: SpecialistHomeViewModel = hiltViewModel(),
) {
    val state = viewModel.state.value
    val navHostController = rememberNavController()
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is SpecialistHomeViewModel.UiEvent.OpenScreen -> {
                    navHostController.navigate(event.route + "specialistId=${specialistId}")
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        bottomBar = {
            BottomNavigation(
                backgroundColor = backgroundColor
            ) {
                state.bottomNavigationItemsList.forEachIndexed { index, item ->
                    BottomNavigationItem(
                        selected = state.selectedItemIndex == index,
                        onClick = {
                            viewModel.onEvent(SpecialistHomeEvent.ItemChange(index, item.route))
                        },
                        label = {
                                Text(text = item.title)
                        },
                        alwaysShowLabel = false,
                        icon = {
                            BadgedBox(
                                badge = {
                                    if(item.badgeCount != null){
                                        Badge{
                                            Text(text = item.badgeCount.toString())
                                        }
                                    }
                                }
                            ) {
                                Icon(
                                    imageVector = if (index == state.selectedItemIndex) {
                                        item.selectedIcon
                                    } else item.unselectedIcon,
                                    contentDescription = item.title
                                )
                            }
                        }
                    )
                }
            }
        }
    ) {innerPadding ->
        NavHost(
            navController = navHostController,
            startDestination = Screen.SpecialistChildListScreen.route + "specialistId={specialistId}",
            Modifier.padding(innerPadding)
        ){
            composable(
                route = Screen.SpecialistChildListScreen.route + "specialistId={specialistId}",
                arguments = listOf(
                    navArgument("specialistId"){ type = NavType.IntType }
                )
            ){
                SpecialistChildListScreen(navController = navController)
            }
            composable(
                route = Screen.SpecialistProfileScreen.route + "specialistId={specialistId}",
                arguments = listOf(
                    navArgument("specialistId"){ type = NavType.IntType }
                )
            ){
                SpecialistProfileScreen(navController = navController)
            }
        }
    }
}