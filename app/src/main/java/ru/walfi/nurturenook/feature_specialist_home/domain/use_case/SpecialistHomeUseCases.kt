package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

data class SpecialistHomeUseCases(
    val getSpecialistUseCase: GetSpecialistUseCase,
    val logOutUseCase: LogOutUseCase,
    val getChildrenUseCase: GetChildrenUseCase,
    val addChildUseCase: AddChildUseCase,
    val getAccountIdUseCase: GetAccountIdUseCase,
    val saveChildIdUseCase: SaveChildIdUseCase,
    val checkPresenceChildUseCase: CheckPresenceChildUseCase
)
