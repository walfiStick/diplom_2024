package ru.walfi.nurturenook.feature_specialist_home.presentation.add_child

import ru.walfi.nurturenook.feature_specialist_home.presentation.util.ChildFieldType

sealed class AddChildEvent {

    data class EnteredNewText(val value: String, val type: ChildFieldType): AddChildEvent()

    object PressedBackButton: AddChildEvent()
    object PressedSaveButton: AddChildEvent()
}