@file:OptIn(ExperimentalMaterial3Api::class)

package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_profile

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Logout
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.BaseInfoRow
import ru.walfi.nurturenook.core.util.InfoRowState
import ru.walfi.nurturenook.ui.theme.backgroundColor

@Composable
fun SpecialistProfileScreen(
    navController: NavController,
    viewModel: SpecialistProfileViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val items = listOf(
        InfoRowState(name = "Фамилия", text = state.specialist.lastName),
        InfoRowState(name = "Имя", text = state.specialist.firstName),
        InfoRowState(name = "Отчество", text = state.specialist.patronymic),
        InfoRowState(name = "Професиия", text = state.specialist.profession),
        InfoRowState(name = "Почта", text = state.specialist.email),
        InfoRowState(name = "Телефон", text = state.specialist.phone),
        InfoRowState(name = "Организация", text = state.specialist.organization),
    )
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true){
        viewModel.eventFlow.collectLatest {event ->
            when(event){
                is SpecialistProfileViewModel.UiEvent.OpenScreen ->{
                    navController.navigate(event.route)
                }
                is SpecialistProfileViewModel.UiEvent.ShowSnackbar ->{
                    scaffoldState.snackbarHostState.showSnackbar(message = event.message)
                }
            }
        }
    }

    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .systemBarsPadding()
            .background(backgroundColor.copy(alpha = 0f)),
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                navigationIcon = {
                    IconButton(onClick = { viewModel.onEvent(SpecialistProfileEvent.PressedLogOutButton) }) {
                        Icon(
                            imageVector = Icons.Default.Logout,
                            contentDescription = "Выйти"
                        )
                    }
                },
                actions = {
                    IconButton(onClick = { /*add edit profile*/ }) {
                        Icon(
                            imageVector = Icons.Default.Edit,
                            contentDescription = "Редактировать профиль"
                        )
                    }
                }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ) {
            items(items){
                Spacer(modifier = Modifier.height(8.dp))
                BaseInfoRow(infoRowState = it)
                Spacer(modifier = Modifier.height(8.dp))
            }
        }
    }
}