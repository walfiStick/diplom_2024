package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_child_list

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.SpecialistHomeUseCases
import javax.inject.Inject

@HiltViewModel
class SpecialistChildListViewModel @Inject constructor(
    private val specialistHomeUseCases: SpecialistHomeUseCases,
) : ViewModel() {

    private val _state = mutableStateOf(
        SpecialistChildListState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var specialistId: Int = -1

    init {
        viewModelScope.launch {
            specialistId = specialistHomeUseCases.getAccountIdUseCase()
        }
        specialistId.let {
            specialistHomeUseCases.getChildrenUseCase(it).onEach { children ->
                _state.value = state.value.copy(
                    children = children
                )
            }.launchIn(viewModelScope)
        }
    }

    fun onEvent(event: SpecialistChildListEvent) {
        when (event) {
            is SpecialistChildListEvent.PressedAddChildButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(
                        UiEvent.OpenScreen(Screen.AddChildScreen.route + "specialistId=${specialistId}")
                    )
                }
            }
            is SpecialistChildListEvent.PressedChild -> {
                viewModelScope.launch {
                    specialistHomeUseCases.saveChildIdUseCase(event.childId)
                    _eventFlow.emit(
                        UiEvent.OpenChildSection(Screen.SpecialistChildSectionScreen, event.childId)
                    )
                }
            }
            is SpecialistChildListEvent.PressedConnectChildButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(
                        UiEvent.OpenScreen(Screen.SpecialistConnectScreen.route)
                    )
                }
            }
        }
    }

    sealed class UiEvent {

        class OpenScreen(val route: String) : UiEvent()
        class ShowSnackbar(val message: String) : UiEvent()
        class OpenChildSection(val screen: Screen, val id: Int) : UiEvent()
    }
}