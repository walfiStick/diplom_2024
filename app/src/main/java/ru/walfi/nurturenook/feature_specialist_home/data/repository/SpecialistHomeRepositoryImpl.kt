package ru.walfi.nurturenook.feature_specialist_home.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.SpecialistToChild
import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.feature_specialist_home.data.data_source.SpecialistHomeDao
import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository
import kotlin.math.acos

class SpecialistHomeRepositoryImpl(
    private val dao: SpecialistHomeDao,
    private val dataStoreManager: DataStoreManager
): SpecialistHomeRepository {

    override suspend fun getSpecialistById(id: Int): Specialist {
        return dao.getSpecialistById(id)
    }

    override suspend fun logOut() {
        dataStoreManager.saveAccountData(
            SavedAccountData(
                role = "",
                accountId = -1
            )
        )
    }

    override suspend fun getSpecialistId(): Int {
        return dataStoreManager.getAccountData().first().accountId
    }

    override suspend fun saveChildId(id: Int) {
        dataStoreManager.saveChildId(id)
    }

    override fun getChildrenBySpecialistId(id: Int): Flow<List<Child>> {
        return dao.getChildrenBySpecialistId(id)
    }

    override suspend fun insertChild(child: Child) {
        dao.insertChild(child)
    }

    override suspend fun insertSpecialistToChild(specialistToChild: SpecialistToChild) {
        dao.insertSpecialistToChild(specialistToChild)
    }

    override suspend fun checkPresenceChild(childCode: String): Int? {
        return dao.checkPresenceChild(childCode)
    }
}