package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository

class GetSpecialistUseCase(
    private val repository: SpecialistHomeRepository
) {

    suspend operator fun invoke(id: Int): Specialist{
        return repository.getSpecialistById(id)
    }
}