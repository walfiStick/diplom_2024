package ru.walfi.nurturenook.feature_specialist_home.presentation.add_child

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.SpecialistHomeUseCases
import ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_child_list.SpecialistChildListViewModel
import ru.walfi.nurturenook.feature_specialist_home.presentation.util.ChildFieldState
import ru.walfi.nurturenook.feature_specialist_home.presentation.util.ChildFieldType
import javax.inject.Inject

@HiltViewModel
class AddChildViewModel @Inject constructor(
    private val specialistHomeUseCases: SpecialistHomeUseCases,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _state = mutableStateOf(
        AddChildState()
    )
    val state = _state
    private var specialistId: Int? = null

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        specialistId = savedStateHandle.get<Int>("specialistId")
    }

    fun onEvent(event: AddChildEvent) {
        when (event) {
            is AddChildEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(
                        UiEvent.NavigateUp
                    )
                }
            }
            is AddChildEvent.PressedSaveButton -> {
                viewModelScope.launch {
                    _state.value = state.value.copy(isLoading = true)
                    try {
                        specialistHomeUseCases.addChildUseCase(
                            Child(
                                firstName = state.value.firstName.testFieldState.text,
                                lastName = state.value.lastName.testFieldState.text,
                                patronymic = state.value.patronymic.testFieldState.text,
                                birthday = state.value.birthday.testFieldState.text,
                                disease = state.value.disease.testFieldState.text,
                                parentFirstName = state.value.parentFirstName.testFieldState.text,
                                parentLastName = state.value.parentLastName.testFieldState.text,
                                parentPatronymic = state.value.parentPatronymic.testFieldState.text,
                                parentPhone = state.value.parentPhone.testFieldState.text,
                                childCode = getRandomString(),
                            ),
                            specialistId
                        )
                        _eventFlow.emit(UiEvent.ShowSuccessAddChild)
                    } catch (e: Exception) {
                        _state.value = state.value.copy(isLoading = false)
                        _eventFlow.emit(
                            UiEvent.ShowSnackbar(message = e.message ?: "Что-то пошло не так")
                        )
                    }
                }
            }
            is AddChildEvent.EnteredNewText -> {
                when (event.type) {
                    ChildFieldType.LastName -> {
                        _state.value = state.value.copy(
                            lastName = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Фамилия"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.FirstName -> {
                        _state.value = state.value.copy(
                            firstName = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Имя"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.Patronymic -> {
                        _state.value = state.value.copy(
                            patronymic = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Отчество"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.Birthday -> {
                        _state.value = state.value.copy(
                            birthday = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Дата рождения"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.Disease -> {
                        _state.value = state.value.copy(
                            disease = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Заболевание"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.ParentLastName -> {
                        _state.value = state.value.copy(
                            parentLastName = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Фамилия родителя"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.ParentFirstName -> {
                        _state.value = state.value.copy(
                            parentFirstName = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Имя родителя"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.ParentPatronymic -> {
                        _state.value = state.value.copy(
                            parentPatronymic = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Отчество родителя"),
                                event.type
                            )
                        )
                    }
                    ChildFieldType.ParentPhone -> {
                        _state.value = state.value.copy(
                            parentPhone = ChildFieldState(
                                TextFieldState(text = event.value, hint = "Номер родителя"),
                                event.type
                            )
                        )
                    }
                }
            }
        }
    }

    private fun getRandomString() : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..10)
            .map { allowedChars.random() }
            .joinToString("")
    }

    sealed class UiEvent() {

        object ShowSuccessAddChild: UiEvent()
        data class ShowSnackbar(val message: String) : UiEvent()
        object NavigateUp : UiEvent()
    }
}