package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_child_list

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.PersonAdd
import androidx.compose.material.icons.outlined.PersonAdd
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.BaseChildCard
import ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_profile.SpecialistProfileViewModel
import ru.walfi.nurturenook.ui.theme.backgroundColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SpecialistChildListScreen(
    navController: NavController,
    viewModel: SpecialistChildListViewModel = hiltViewModel()
){
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true){
        viewModel.eventFlow.collectLatest { event ->
            when(event){
                is SpecialistChildListViewModel.UiEvent.OpenScreen ->{
                    navController.navigate(event.route)
                }
                is SpecialistChildListViewModel.UiEvent.ShowSnackbar ->{
                    scaffoldState.snackbarHostState.showSnackbar(event.message)
                }
                is SpecialistChildListViewModel.UiEvent.OpenChildSection -> {
                    navController.navigate(event.screen.route + "childId=${event.id}")
                }
            }
        }
    }

    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .systemBarsPadding()
            .background(backgroundColor.copy(alpha = 0f)),
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                actions = {
                    IconButton(onClick = { viewModel.onEvent(SpecialistChildListEvent.PressedConnectChildButton) }) {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = "Добавить ребенка"
                        )
                    }
                    IconButton(onClick = { viewModel.onEvent(SpecialistChildListEvent.PressedAddChildButton) }) {
                        Icon(
                            imageVector = Icons.Default.PersonAdd,
                            contentDescription = "Добавить ребенка"
                        )
                    }
                }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .fillMaxSize()
        ) {
            items(state.children){child ->
                Spacer(modifier = Modifier.height(4.dp))
                BaseChildCard(
                    lastName = child.lastName,
                    firstName = child.firstName,
                    parentPhone = child.parentPhone,
                    onClick = {
                        viewModel.onEvent(SpecialistChildListEvent.PressedChild(child.id!!))
                    }
                )
                Spacer(modifier = Modifier.height(4.dp))
            }
        }
    }
}