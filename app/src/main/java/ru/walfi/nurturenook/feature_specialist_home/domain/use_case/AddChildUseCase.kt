package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import android.database.sqlite.SQLiteException
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.InvalidChildException
import ru.walfi.nurturenook.core.model.SpecialistToChild
import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository

class AddChildUseCase(
    private val repository: SpecialistHomeRepository
) {

    @Throws(InvalidChildException::class)
    suspend operator fun invoke(child: Child, specialistId: Int?){
        if(child.lastName.isBlank()){
            throw InvalidChildException("Поле 'Фамилия' не может быть пустым")
        }
        if(child.firstName.isBlank()){
            throw InvalidChildException("Поле 'Имя' не может быть пустым")
        }
        if(child.patronymic.isBlank()){
            throw InvalidChildException("Поле 'Отчество' не может быть пустым")
        }
        if(child.birthday.isBlank()){
            throw InvalidChildException("Поле 'Дата рождения' не может быть пустым")
        }
        if(child.disease.isBlank()){
            throw InvalidChildException("Поле 'Заболевание' не может быть пустым")
        }
        if(child.parentLastName.isBlank()){
            throw InvalidChildException("Поле 'Фамилия родителя' не может быть пустым")
        }
        if(child.parentFirstName.isBlank()){
            throw InvalidChildException("Поле 'Имя родителя' не может быть пустым")
        }
        if(child.parentPatronymic.isBlank()){
            throw InvalidChildException("Поле 'Отчество родителя' не может быть пустым")
        }
        if(child.parentPhone.isBlank()){
            throw InvalidChildException("Поле 'Номер родителя' не может быть пустым")
        }
        if(specialistId == null){
            throw InvalidChildException("Не выявлен специалист")
        }
        try {
            repository.insertChild(child)
            repository.insertSpecialistToChild(
                SpecialistToChild(child.childCode, specialistId)
            )
        }catch(e: SQLiteException){
            throw SQLiteException("Ошибка регистрации. Попробуйте повторить ошибку")
        }
    }
}