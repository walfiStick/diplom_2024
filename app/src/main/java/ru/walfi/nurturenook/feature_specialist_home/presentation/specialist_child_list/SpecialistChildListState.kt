package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_child_list

import ru.walfi.nurturenook.core.model.Child

data class SpecialistChildListState(
    val toolbarText: String = "Список детей",
    val children: List<Child> = emptyList()
)