package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository

class GetAccountIdUseCase(
    private val repository: SpecialistHomeRepository
) {

    suspend operator fun invoke(): Int {
        return repository.getSpecialistId()
    }
}