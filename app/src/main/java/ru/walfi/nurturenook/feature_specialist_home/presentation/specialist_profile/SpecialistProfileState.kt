package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_profile

import ru.walfi.nurturenook.core.model.Specialist

data class SpecialistProfileState(
    val toolbarText: String = "Профиль",
    val specialist: Specialist = Specialist(
        lastName = "",
        firstName = "",
        patronymic = "",
        profession = "",
        email = "",
        phone = "",
        organization = "",
        password = ""
    )
)