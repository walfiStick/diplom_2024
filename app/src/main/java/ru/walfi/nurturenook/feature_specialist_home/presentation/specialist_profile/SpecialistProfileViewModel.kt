package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_profile

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.SpecialistHomeUseCases
import javax.inject.Inject

@HiltViewModel
class SpecialistProfileViewModel @Inject constructor(
    private val specialistHomeUseCases: SpecialistHomeUseCases,
    savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _state = mutableStateOf(
        SpecialistProfileState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        savedStateHandle.get<Int>("specialistId")?.let {
            viewModelScope.launch {
                specialistHomeUseCases.getSpecialistUseCase(it).let {
                    _state.value = state.value.copy(
                        specialist = Specialist(
                            firstName = it.firstName,
                            lastName = it.lastName,
                            patronymic = it.patronymic,
                            profession = it.profession,
                            email = it.email,
                            phone = it.phone,
                            organization = it.organization,
                            password = it.password,
                            id = it.id
                        )
                    )
                }
            }
        }
    }

    fun onEvent(event: SpecialistProfileEvent){
        when(event){
            is SpecialistProfileEvent.PressedLogOutButton -> {
                viewModelScope.launch {
                    specialistHomeUseCases.logOutUseCase()
                    _eventFlow.emit(
                        UiEvent.OpenScreen(Screen.AuthorizationScreen.route)
                    )
                }
            }
            is SpecialistProfileEvent.PressedEditProfileButton -> {/*add logic edit profile data*/}
        }
    }

    sealed class UiEvent{
        class OpenScreen(val route: String): UiEvent()
        data class ShowSnackbar(val message: String): UiEvent()
    }
}