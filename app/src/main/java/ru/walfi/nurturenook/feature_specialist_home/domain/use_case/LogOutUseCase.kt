package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository

class LogOutUseCase(
    private val repository: SpecialistHomeRepository
) {

    suspend operator fun invoke(){
        repository.logOut()
    }
}