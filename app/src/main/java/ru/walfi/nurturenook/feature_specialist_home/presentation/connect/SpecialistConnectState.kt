package ru.walfi.nurturenook.feature_specialist_home.presentation.connect

import ru.walfi.nurturenook.core.util.TextFieldState

data class SpecialistConnectState(
    val toolbarText: String = "Добавить ребенка",
    val childCode: TextFieldState = TextFieldState(hint = "Уникальный код"),
    val isLoading: Boolean = false
)