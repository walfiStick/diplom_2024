package ru.walfi.nurturenook.feature_specialist_home.presentation.home

sealed class SpecialistHomeEvent{

    class ItemChange(val index: Int, val route: String): SpecialistHomeEvent()
}