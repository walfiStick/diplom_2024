package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import ru.walfi.nurturenook.core.model.SpecialistToChild
import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository

class CheckPresenceChildUseCase(
    private val repository: SpecialistHomeRepository
) {

    suspend operator fun invoke(childCode: String, specialistId: Int): Int? {
        val childId = repository.checkPresenceChild(childCode)
        return if(childId == null){
            null
        }else{
            repository.insertSpecialistToChild(
                SpecialistToChild(
                    childCode = childCode,
                    specialistId = specialistId
                )
            )
            childId
        }
    }
}