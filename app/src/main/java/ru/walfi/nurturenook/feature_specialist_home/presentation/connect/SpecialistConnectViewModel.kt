package ru.walfi.nurturenook.feature_specialist_home.presentation.connect

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_specialist_home.domain.use_case.SpecialistHomeUseCases
import javax.inject.Inject

@HiltViewModel
class SpecialistConnectViewModel @Inject constructor(
    private val specialistHomeUseCases: SpecialistHomeUseCases
): ViewModel() {

    private val _state = mutableStateOf(
        SpecialistConnectState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var specialistId: Int? = null

    init {
        viewModelScope.launch {
            specialistId = specialistHomeUseCases.getAccountIdUseCase()
        }
    }

    fun onEvent(event: SpecialistConnectEvent){
        when(event){
            is SpecialistConnectEvent.PressedConnectButton -> {
                _state.value = state.value.copy(isLoading = true)
                viewModelScope.launch {
                    val res = specialistHomeUseCases.checkPresenceChildUseCase(_state.value.childCode.text, specialistId!!)
                    if (res == null){
                        _state.value = state.value.copy(isLoading = false)
                        _eventFlow.emit(UiEvent.ShowSnackbar("Ошибка. Проверьте правильность кода"))
                    }else{
                        _eventFlow.emit(UiEvent.ShowSuccessConnect)
                    }
                }
            }
            is SpecialistConnectEvent.EnteredNewValue -> {
                _state.value = state.value.copy(
                    childCode = TextFieldState(
                        hint = "Уникальный код",
                        text = event.text
                    )
                )
            }
            is SpecialistConnectEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
        }
    }

    sealed class UiEvent{
        object ShowSuccessConnect: UiEvent()
        data class ShowSnackbar(val message: String) : UiEvent()
        object NavigateUp : UiEvent()
    }
}