package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_child_list

sealed class SpecialistChildListEvent {

    object PressedAddChildButton: SpecialistChildListEvent()

    object PressedConnectChildButton: SpecialistChildListEvent()

    class PressedChild(val childId: Int): SpecialistChildListEvent()
}