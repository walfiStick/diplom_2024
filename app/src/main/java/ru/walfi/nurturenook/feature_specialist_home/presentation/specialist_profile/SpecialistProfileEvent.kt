package ru.walfi.nurturenook.feature_specialist_home.presentation.specialist_profile

sealed class SpecialistProfileEvent {

    object PressedLogOutButton: SpecialistProfileEvent()

    object PressedEditProfileButton: SpecialistProfileEvent()
}