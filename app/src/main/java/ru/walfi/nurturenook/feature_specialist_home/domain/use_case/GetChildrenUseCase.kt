package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository


class GetChildrenUseCase(
    private val repository: SpecialistHomeRepository
) {

    operator fun invoke(id: Int): Flow<List<Child>> {
        return repository.getChildrenBySpecialistId(id)
    }
}