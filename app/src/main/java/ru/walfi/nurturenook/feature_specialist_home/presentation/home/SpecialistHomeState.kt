package ru.walfi.nurturenook.feature_specialist_home.presentation.home

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.outlined.AccountCircle
import androidx.compose.material.icons.outlined.List
import ru.walfi.nurturenook.core.util.BottomNavigationItem
import ru.walfi.nurturenook.core.util.Screen

data class SpecialistHomeState(
    val bottomNavigationItemsList: List<BottomNavigationItem> = listOf(
        BottomNavigationItem(
            title = "Список",
            selectedIcon = Icons.Filled.List,
            unselectedIcon = Icons.Outlined.List,
            route = Screen.SpecialistChildListScreen.route
        ),
        BottomNavigationItem(
            title = "Профиль",
            selectedIcon = Icons.Filled.AccountCircle,
            unselectedIcon = Icons.Outlined.AccountCircle,
            route = Screen.SpecialistProfileScreen.route
        ),
    ),
    val selectedItemIndex: Int = 0
)