package ru.walfi.nurturenook.feature_specialist_home.presentation.util

import ru.walfi.nurturenook.core.util.TextFieldState

class ChildFieldState(
    val testFieldState: TextFieldState,
    val type: ChildFieldType
)

enum class ChildFieldType {
    LastName,
    FirstName,
    Patronymic,
    Birthday,
    Disease,
    ParentFirstName,
    ParentLastName,
    ParentPatronymic,
    ParentPhone,
}