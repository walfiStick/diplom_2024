package ru.walfi.nurturenook.feature_specialist_home.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.ParentToChild
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.SpecialistToChild

interface SpecialistHomeRepository {

    suspend fun getSpecialistById(id: Int): Specialist

    suspend fun logOut()

    suspend fun getSpecialistId(): Int

    suspend fun saveChildId(id: Int)

    fun getChildrenBySpecialistId(id: Int): Flow<List<Child>>

    suspend fun insertChild(child: Child)

    suspend fun insertSpecialistToChild(specialistToChild: SpecialistToChild)

    suspend fun checkPresenceChild(childCode: String): Int?
}