package ru.walfi.nurturenook.feature_specialist_home.domain.use_case

import ru.walfi.nurturenook.feature_specialist_home.domain.repository.SpecialistHomeRepository

class SaveChildIdUseCase(
    private val repository: SpecialistHomeRepository
) {

    suspend operator fun invoke(id: Int){
        repository.saveChildId(id)
    }
}