package ru.walfi.nurturenook.feature_specialist_home.data.data_source

import android.database.sqlite.SQLiteException
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.SpecialistToChild

@Dao
interface SpecialistHomeDao {

    @Query("SELECT * FROM specialist WHERE id = :id")
    @Throws(SQLiteException::class)
    suspend fun getSpecialistById(id: Int): Specialist

    @Query("SELECT * FROM child " +
            "JOIN specialisttochild ON child.childCode = specialisttochild.childCode " +
            "WHERE specialisttochild.specialistId = :id")
    @Throws(SQLiteException::class)
    fun getChildrenBySpecialistId(id: Int): Flow<List<Child>>

    @Insert
    @Throws(SQLiteException::class)
    suspend fun insertChild(child: Child)

    @Insert
    suspend fun insertSpecialistToChild(specialistToChild: SpecialistToChild)

    @Query("SELECT id FROM child WHERE childCode = :childCode")
    suspend fun checkPresenceChild(childCode: String): Int?
}