package ru.walfi.nurturenook.feature_specialist_home.presentation.add_child

import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_specialist_home.presentation.util.ChildFieldState
import ru.walfi.nurturenook.feature_specialist_home.presentation.util.ChildFieldType

data class AddChildState(
    val toolbarText: String = "Добавление ребенка",
    val lastName: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Фамилия"),
        ChildFieldType.LastName
    ),
    val firstName: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Имя"),
        ChildFieldType.FirstName
    ),
    val patronymic: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Отчество"),
        ChildFieldType.Patronymic
    ),
    val birthday: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Дата рождения"),
        ChildFieldType.Birthday
    ),
    val disease: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Заболевание"),
        ChildFieldType.Disease
    ),
    val parentLastName: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Фамилия родителя"),
        ChildFieldType.ParentLastName
    ),
    val parentFirstName: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Имя родителя"),
        ChildFieldType.ParentFirstName
    ),
    val parentPatronymic: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Отчество родителя"),
        ChildFieldType.ParentPatronymic
    ),
    val parentPhone: ChildFieldState = ChildFieldState(
        TextFieldState(hint = "Номер родителя"),
        ChildFieldType.ParentPhone
    ),
    val isLoading: Boolean = false
)
