package ru.walfi.nurturenook.feature_specialist_home.presentation.connect

sealed class SpecialistConnectEvent{

    class EnteredNewValue(val text: String): SpecialistConnectEvent()

    object PressedConnectButton: SpecialistConnectEvent()

    object PressedBackButton: SpecialistConnectEvent()
}