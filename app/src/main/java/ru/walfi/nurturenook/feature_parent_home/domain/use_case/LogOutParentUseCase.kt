package ru.walfi.nurturenook.feature_parent_home.domain.use_case

import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class LogOutParentUseCase(
    private val repository: ParentHomeRepository
) {

    suspend operator fun invoke(){
        repository.logOut()
    }
}