package ru.walfi.nurturenook.feature_parent_home.domain.use_case

data class ParentHomeUseCases(
    val getAccountIdUseCase: GetAccountIdUseCase,
    val getChildrenForParentUseCase: GetChildrenForParentUseCase,
    val getParentUseCase: GetParentUseCase,
    val logOutParentUseCase: LogOutParentUseCase,
    val checkPresenceChildUseCase: CheckPresenceChildUseCase,
    val saveChildForParentUseCase: SaveChildForParentUseCase
)
