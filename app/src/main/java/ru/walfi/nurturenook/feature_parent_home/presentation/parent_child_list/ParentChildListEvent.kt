package ru.walfi.nurturenook.feature_parent_home.presentation.parent_child_list

sealed class ParentChildListEvent{

    object PressedConnectToChild: ParentChildListEvent()

    class PressedChild(val childId: Int): ParentChildListEvent()
}
