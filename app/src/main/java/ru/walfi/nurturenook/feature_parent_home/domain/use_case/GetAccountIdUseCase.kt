package ru.walfi.nurturenook.feature_parent_home.domain.use_case

import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class GetAccountIdUseCase(
    private val repository: ParentHomeRepository
) {

    suspend operator fun invoke(): Int{
        return repository.getParentId()
    }
}