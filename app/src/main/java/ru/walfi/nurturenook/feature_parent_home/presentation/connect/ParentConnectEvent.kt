package ru.walfi.nurturenook.feature_parent_home.presentation.connect

sealed class ParentConnectEvent{

    class EnteredNewValue(val text: String): ParentConnectEvent()

    object PressedConnectButton: ParentConnectEvent()

    object PressedBackButton: ParentConnectEvent()
}
