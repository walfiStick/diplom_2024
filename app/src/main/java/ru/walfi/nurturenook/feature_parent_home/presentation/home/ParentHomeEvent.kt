package ru.walfi.nurturenook.feature_parent_home.presentation.home

sealed class ParentHomeEvent{

    class ItemChange(val index: Int, val route: String): ParentHomeEvent()
}
