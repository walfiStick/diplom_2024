package ru.walfi.nurturenook.feature_parent_home.presentation.parent_child_list

import ru.walfi.nurturenook.core.model.Child

data class ParentChildListState(
    val toolbarText: String = "Список детей",
    val children: List<Child> = emptyList()
)
