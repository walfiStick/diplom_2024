package ru.walfi.nurturenook.feature_parent_home.presentation.parent_profile

import ru.walfi.nurturenook.core.model.Parent

data class ParentProfileState(
    val toolbarText: String = "Профиль",
    val parent: Parent = Parent(
        firstName = "",
        lastName = "",
        patronymic = "",
        email = "",
        password = ""
    )
)
