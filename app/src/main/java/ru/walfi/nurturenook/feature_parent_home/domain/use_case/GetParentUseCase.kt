package ru.walfi.nurturenook.feature_parent_home.domain.use_case

import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class GetParentUseCase(
    private val repository: ParentHomeRepository
) {

    suspend operator fun invoke(id: Int): Parent {
        return repository.getParentById(id)
    }
}