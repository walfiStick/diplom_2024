package ru.walfi.nurturenook.feature_parent_home.presentation.parent_profile

sealed class ParentProfileEvent{

    object PressedEditProfileButton: ParentProfileEvent()

    object PressedLogOutButton: ParentProfileEvent()
}
