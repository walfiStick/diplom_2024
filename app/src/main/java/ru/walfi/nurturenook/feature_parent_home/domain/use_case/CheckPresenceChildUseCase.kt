package ru.walfi.nurturenook.feature_parent_home.domain.use_case

import ru.walfi.nurturenook.core.model.ParentToChild
import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class CheckPresenceChildUseCase(
    private val repository: ParentHomeRepository
) {

    suspend operator fun invoke(childCode: String, parentId: Int): Int? {
        val childId = repository.checkPresenceChild(childCode)
        return if(childId == null){
            null
        }else{
            repository.insertParentToChild(
                ParentToChild(
                    childCode = childCode,
                    parentId = parentId
                )
            )
            childId
        }
    }
}