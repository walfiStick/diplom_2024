package ru.walfi.nurturenook.feature_parent_home.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.ParentToChild

interface ParentHomeRepository {

    suspend fun getParentById(id: Int): Parent

    suspend fun logOut()

    suspend fun getParentId(): Int

    suspend fun saveChild(id: Int)

    fun getChildrenByParentId(id: Int): Flow<List<Child>>

    suspend fun insertParentToChild(parentToChild: ParentToChild)

    suspend fun checkPresenceChild(childCode: String): Int?

}