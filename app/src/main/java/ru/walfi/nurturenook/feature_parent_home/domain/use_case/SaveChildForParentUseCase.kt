package ru.walfi.nurturenook.feature_parent_home.domain.use_case

import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class SaveChildForParentUseCase(
    private val repository: ParentHomeRepository
) {

    suspend operator fun invoke(id: Int){
        repository.saveChild(id)
    }
}