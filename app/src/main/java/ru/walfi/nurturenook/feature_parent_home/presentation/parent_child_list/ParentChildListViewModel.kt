package ru.walfi.nurturenook.feature_parent_home.presentation.parent_child_list

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.ParentHomeUseCases
import javax.inject.Inject

@HiltViewModel
class ParentChildListViewModel @Inject constructor(
    private val parentHomeUseCases: ParentHomeUseCases
): ViewModel() {

    private val _state = mutableStateOf(
        ParentChildListState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var parentId: Int? = null

    init {
        viewModelScope.launch {
            parentId = parentHomeUseCases.getAccountIdUseCase()
        }
        parentId?.let {
            parentHomeUseCases.getChildrenForParentUseCase(it).onEach { children ->
                _state.value = state.value.copy(
                    children = children
                )
            }.launchIn(viewModelScope)
        }
    }

    fun onEvent(event: ParentChildListEvent){
        when(event){
            is ParentChildListEvent.PressedConnectToChild -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.OpenScreen(Screen.ParentConnectScreen.route))
                }
            }
            is ParentChildListEvent.PressedChild -> {
                viewModelScope.launch {
                    parentHomeUseCases.saveChildForParentUseCase(event.childId)
                    _eventFlow.emit(UiEvent.OpenScreen(Screen.ParentChildSectionScreen.route))
                }
            }
        }
    }

    sealed class UiEvent{
        class OpenScreen(val route: String) : UiEvent()
        class ShowSnackbar(val message: String) : UiEvent()
    }
}
