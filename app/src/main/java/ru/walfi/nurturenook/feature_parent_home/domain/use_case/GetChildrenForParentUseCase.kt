package ru.walfi.nurturenook.feature_parent_home.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class GetChildrenForParentUseCase(
    private val repository: ParentHomeRepository
) {

    operator fun invoke(id: Int): Flow<List<Child>>{
        return repository.getChildrenByParentId(id)
    }
}