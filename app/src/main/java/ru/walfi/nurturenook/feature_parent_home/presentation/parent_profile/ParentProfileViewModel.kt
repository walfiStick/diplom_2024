package ru.walfi.nurturenook.feature_parent_home.presentation.parent_profile

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.ParentHomeUseCases
import javax.inject.Inject

@HiltViewModel
class ParentProfileViewModel @Inject constructor(
    private val parentHomeUseCases: ParentHomeUseCases
) : ViewModel() {

    private val _state = mutableStateOf(
        ParentProfileState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var parentId: Int? = null

    init {
        viewModelScope.launch {
            parentId = parentHomeUseCases.getAccountIdUseCase()
            parentId?.let {
                _state.value = state.value.copy(
                    parent = parentHomeUseCases.getParentUseCase(parentId!!)
                )
            }
        }
    }

    fun onEvent(event: ParentProfileEvent) {
        when (event) {
            is ParentProfileEvent.PressedLogOutButton -> {
                viewModelScope.launch {
                    parentHomeUseCases.logOutParentUseCase()
                    _eventFlow.emit(
                        UiEvent.OpenScreen(Screen.AuthorizationScreen.route)
                    )
                }
            }
            is ParentProfileEvent.PressedEditProfileButton -> {/*add logic edit profile data*/
            }
        }
    }

    sealed class UiEvent {
        class OpenScreen(val route: String) : UiEvent()
        data class ShowSnackbar(val message: String) : UiEvent()
    }
}