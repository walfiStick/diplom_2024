package ru.walfi.nurturenook.feature_parent_home.data.data_source

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.ParentToChild

@Dao
interface ParentHomeDao {

    @Query("SELECT * FROM parent WHERE id = :id")
    suspend fun getParentById(id: Int): Parent

    @Query("SELECT * FROM child " +
            "JOIN parenttochild ON child.childCode = parenttochild.childCode " +
            "WHERE parenttochild.parentId = :id")
    fun getChildrenByParentId(id: Int): Flow<List<Child>>

    @Insert
    suspend fun insertParentToChild(parentToChild: ParentToChild)

    @Query("SELECT id FROM child WHERE childCode = :childCode")
    suspend fun checkPresenceChild(childCode: String): Int?
}