package ru.walfi.nurturenook.feature_parent_home.presentation.connect

import ru.walfi.nurturenook.core.util.TextFieldState

data class ParentConnectState(
    val toolbarText: String = "Добавить ребенка",
    val childCode: TextFieldState = TextFieldState(hint = "Уникальный код"),
    val isLoading: Boolean = false
)
