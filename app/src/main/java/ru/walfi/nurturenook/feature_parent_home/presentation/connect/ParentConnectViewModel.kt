package ru.walfi.nurturenook.feature_parent_home.presentation.connect

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_parent_home.domain.use_case.ParentHomeUseCases
import javax.inject.Inject

@HiltViewModel
class ParentConnectViewModel @Inject constructor(
    private val parentHomeUseCases: ParentHomeUseCases
): ViewModel() {

    private val _state = mutableStateOf(
        ParentConnectState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var parentId: Int? = null

    init {
        viewModelScope.launch {
            parentId = parentHomeUseCases.getAccountIdUseCase()
        }
    }

    fun onEvent(event: ParentConnectEvent){
        when(event){
            is ParentConnectEvent.PressedConnectButton -> {
                _state.value = state.value.copy(isLoading = true)
                viewModelScope.launch {
                    val res = parentHomeUseCases.checkPresenceChildUseCase(_state.value.childCode.text, parentId!!)
                    if (res == null){
                        _state.value = state.value.copy(isLoading = false)
                        _eventFlow.emit(UiEvent.ShowSnackbar("Ошибка. Проверьте правильность кода"))
                    }else{
                        _eventFlow.emit(UiEvent.ShowSuccessConnect)
                    }
                }
            }
            is ParentConnectEvent.EnteredNewValue -> {
                _state.value = state.value.copy(
                    childCode = TextFieldState(
                        hint = "Уникальный код",
                        text = event.text
                    )
                )
            }
            is ParentConnectEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
        }
    }

    sealed class UiEvent{
        object ShowSuccessConnect: UiEvent()
        data class ShowSnackbar(val message: String) : UiEvent()
        object NavigateUp : UiEvent()
    }
}