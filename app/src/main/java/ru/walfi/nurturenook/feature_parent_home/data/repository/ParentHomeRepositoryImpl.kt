package ru.walfi.nurturenook.feature_parent_home.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.ParentToChild
import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.feature_parent_home.data.data_source.ParentHomeDao
import ru.walfi.nurturenook.feature_parent_home.domain.repository.ParentHomeRepository

class ParentHomeRepositoryImpl(
    private val dao: ParentHomeDao,
    private val dataStoreManager: DataStoreManager
): ParentHomeRepository {

    override suspend fun getParentById(id: Int): Parent {
        return dao.getParentById(id)
    }

    override suspend fun logOut() {
        dataStoreManager.saveAccountData(
            SavedAccountData(
                role = "",
                accountId = -1
            )
        )
    }

    override suspend fun getParentId(): Int {
        return dataStoreManager.getAccountData().first().accountId
    }

    override suspend fun saveChild(id: Int) {
        dataStoreManager.saveChildId(id)
    }

    override fun getChildrenByParentId(id: Int): Flow<List<Child>> {
        return dao.getChildrenByParentId(id)
    }

    override suspend fun insertParentToChild(parentToChild: ParentToChild) {
        dao.insertParentToChild(parentToChild)
    }

    override suspend fun checkPresenceChild(childCode: String): Int? {
        return dao.checkPresenceChild(childCode)
    }
}