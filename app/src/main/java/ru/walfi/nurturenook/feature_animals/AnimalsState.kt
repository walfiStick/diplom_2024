package ru.walfi.nurturenook.feature_animals

import androidx.annotation.DrawableRes
import ru.walfi.nurturenook.R

data class AnimalsState(
    val toolbarText: String = "Животные",
    val animalsList: List<Animal> = listOf(
        Animal(
            picture = R.drawable.dog,
            name = "Собака"
        ),
        Animal(
            picture = R.drawable.cat,
            name = "Кошка"
        ),
        Animal(
            picture = R.drawable.cow,
            name = "Корова"
        ),
        Animal(
            picture = R.drawable.raccoon,
            name = "Енот"
        ),
        Animal(
            picture = R.drawable.bear,
            name = "Медведь"
        ),
        Animal(
            picture = R.drawable.eagle,
            name = "Орел"
        ),
        Animal(
            picture = R.drawable.hare,
            name = "Заяц"
        ),
        Animal(
            picture = R.drawable.lion,
            name = "Лев"
        ),
        Animal(
            picture = R.drawable.parrot,
            name = "Попугай"
        ),
        Animal(
            picture = R.drawable.wolf,
            name = "Волк"
        ),
        Animal(
            picture = R.drawable.fox,
            name = "Лиса"
        ),
        Animal(
            picture = R.drawable.horse,
            name = "Лошадь"
        ),
        Animal(
            picture = R.drawable.camel,
            name = "Верблюд"
        ),
        Animal(
            picture = R.drawable.goat,
            name = "Козел"
        ),
        Animal(
            picture = R.drawable.hamster,
            name = "Хомяк"
        ),
        Animal(
            picture = R.drawable.ram,
            name = "Баран"
        ),
        Animal(
            picture = R.drawable.bison,
            name = "Бизон"
        ),
        Animal(
            picture = R.drawable.deer,
            name = "Олень"
        ),
        Animal(
            picture = R.drawable.elephant,
            name = "Слон"
        ),
        Animal(
            picture = R.drawable.giraffe,
            name = "Жираф"
        ),
        Animal(
            picture = R.drawable.snake,
            name = "Змея"
        ),
        Animal(
            picture = R.drawable.zebra,
            name = "Зебра"
        ),
        Animal(
            picture = R.drawable.leopard,
            name = "Леопард"
        ),
        Animal(
            picture = R.drawable.tiger,
            name = "Тигр"
        ),
    )
)

data class Animal(
    @DrawableRes val picture: Int,
    val name: String
)