package ru.walfi.nurturenook.feature_animals

import android.content.Context

sealed class AnimalsEvent {

    class PressedAnimal(val animal: Animal, val context: Context): AnimalsEvent()
    object PressedBackButton: AnimalsEvent()
}