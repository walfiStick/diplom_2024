package ru.walfi.nurturenook.feature_animals

import android.speech.tts.TextToSpeech
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class AnimalsViewModel @Inject constructor(): ViewModel() {

    private val _state = mutableStateOf(AnimalsState())
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: AnimalsEvent){
        when(event){
            is AnimalsEvent.PressedAnimal ->{
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.ShowToast(message = event.animal.name))
                }
                var textToSpeech: TextToSpeech? = null
                textToSpeech = TextToSpeech(event.context){
                    if(it == TextToSpeech.SUCCESS){
                        textToSpeech?.let {text ->
                            text.setSpeechRate(1f)
                            text.speak(
                                event.animal.name,
                                TextToSpeech.QUEUE_ADD,
                                null,
                                null
                            )
                        }
                    }
                }
            }
            is AnimalsEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigationUp)
                }
            }
        }
    }

    sealed class UiEvent{
        data class ShowToast(val message: String): UiEvent()
        object NavigationUp: UiEvent()
    }
}