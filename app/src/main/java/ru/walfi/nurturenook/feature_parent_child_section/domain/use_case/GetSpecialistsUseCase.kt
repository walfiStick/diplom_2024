package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class GetSpecialistsUseCase(
    private val repository: ParentChildSectionRepository
) {

    suspend operator fun invoke(id: Int): Flow<List<Specialist>>{
        return repository.getSpecialistsByChildCode(repository.getChildCodeByChildId(id))
    }
}