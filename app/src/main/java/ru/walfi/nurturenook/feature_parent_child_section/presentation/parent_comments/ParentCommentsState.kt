package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_comments

import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.feature_parent_child_section.data.util.CommentData

data class ParentCommentsState(
    val toolbarText: String = "Комментарии",
    val commentList: List<CommentData> = emptyList()
)