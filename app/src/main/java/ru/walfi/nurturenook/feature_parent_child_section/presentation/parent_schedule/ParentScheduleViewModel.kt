package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_schedule

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.ParentChildSectionUseCases
import javax.inject.Inject

@HiltViewModel
class ParentScheduleViewModel @Inject constructor(
    private val parentChildSectionUseCases: ParentChildSectionUseCases
): ViewModel() {

    private val _state = mutableStateOf(ParentScheduleState())
    val state = _state

    private var childId: Int? = null

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        viewModelScope.launch {
            childId = parentChildSectionUseCases.getChildIdParentUseCase()
            parentChildSectionUseCases.getTasksUseCase(childId!!).onEach {
                _state.value = state.value.copy(
                    tasksList = it
                )
            }.launchIn(viewModelScope)
        }
    }

    fun onEvent(event: ParentScheduleEvent){
        when(event){
            is ParentScheduleEvent.PressedDeleteButton -> {
                viewModelScope.launch {
                    parentChildSectionUseCases.deleteTaskUseCase(event.task)
                }
            }
            is ParentScheduleEvent.PressedSaveButton -> {
                viewModelScope.launch {
                    try {
                        parentChildSectionUseCases.addTaskUseCase(
                            Task(
                                description = _state.value.currentTask.text,
                                childId = childId!!,
                                executeFlag = false
                            )
                        )
                        _state.value = state.value.copy(
                            isCreation = false,
                            currentTask = TextFieldState(
                                hint = "Введите задачу",
                                text = ""
                            )
                        )
                    }catch (e: Exception){
                        _eventFlow.emit(UiEvent.ShowToast(e.message ?: "Что-то пошло не так"))
                    }
                }
            }
            is ParentScheduleEvent.EnteredNewValue -> {
                _state.value = state.value.copy(
                    currentTask = TextFieldState(
                        hint = "Введите задачу",
                        text = event.value
                    )
                )
            }
            is ParentScheduleEvent.PressedAddTaskButton -> {
                _state.value = state.value.copy(isCreation = true)
            }
            is ParentScheduleEvent.PressedCancelButton -> {
                _state.value = state.value.copy(
                    isCreation = false,
                    currentTask = TextFieldState(
                        hint = "Введите задачу",
                        text = ""
                    )
                )
            }
        }
    }

    sealed class UiEvent{

        class ShowToast(val message: String): UiEvent()
    }
}