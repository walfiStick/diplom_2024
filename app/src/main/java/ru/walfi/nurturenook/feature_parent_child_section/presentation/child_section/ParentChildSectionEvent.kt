package ru.walfi.nurturenook.feature_parent_child_section.presentation.child_section

sealed class ParentChildSectionEvent {

    class ItemChange(val index: Int, val route: String): ParentChildSectionEvent()
}