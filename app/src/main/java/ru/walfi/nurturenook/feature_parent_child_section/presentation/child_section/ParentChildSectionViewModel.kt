package ru.walfi.nurturenook.feature_parent_child_section.presentation.child_section

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ParentChildSectionViewModel @Inject constructor(): ViewModel(){

    private val _state = mutableStateOf(ParentChildSectionState())
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: ParentChildSectionEvent){
        when(event){
            is ParentChildSectionEvent.ItemChange -> {
                if(_state.value.selectedItemIndex != event.index){
                    _state.value = state.value.copy(selectedItemIndex = event.index)
                    viewModelScope.launch {
                        _eventFlow.emit(UiEvent.OpenScreen(event.route))
                    }
                }
            }
        }
    }

    sealed class UiEvent{
        class OpenScreen(val route: String): UiEvent()
    }
}