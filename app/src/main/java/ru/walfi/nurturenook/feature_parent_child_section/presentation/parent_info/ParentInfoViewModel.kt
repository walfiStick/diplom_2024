package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_info

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.ParentChildSectionUseCases
import javax.inject.Inject

@HiltViewModel
class ParentInfoViewModel @Inject constructor(
    private val parentChildSectionUseCases: ParentChildSectionUseCases
): ViewModel() {

    private val _state = mutableStateOf(ParentInfoState())
    val state = _state

    private var childId: Int? = null

    init {
        viewModelScope.launch {
            childId = parentChildSectionUseCases.getChildIdParentUseCase()
            parentChildSectionUseCases.getSpecialistsUseCase(childId!!).onEach {
                _state.value = state.value.copy(
                    specialistsList = it
                )
            }.launchIn(viewModelScope)
        }
    }
}