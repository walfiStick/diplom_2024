package ru.walfi.nurturenook.feature_parent_child_section.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_parent_child_section.data.util.CommentData

interface ParentChildSectionRepository {

    fun getCommentsByChildId(id: Int): Flow<List<CommentData>>

    suspend fun getChildId(): Int

    fun getSpecialistsByChildCode(childCode: String): Flow<List<Specialist>>

    suspend fun getChildCodeByChildId(id: Int): String

    suspend fun insertTask(task: Task)

    fun getTasksByChildId(childId: Int): Flow<List<Task>>

    suspend fun deleteTask(task: Task)
}