package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_info.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.ui.theme.commentCardsColor

@Composable
fun SpecialistCard(
    specialist: Specialist,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .clip(shape = RoundedCornerShape(8.dp))
            .background(commentCardsColor)
            .clip(shape = RoundedCornerShape(8.dp))
            .padding(4.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(text = specialist.lastName)
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = specialist.firstName)
        }
        Spacer(modifier = Modifier.height(4.dp))
        Text(text = specialist.patronymic)
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = specialist.profession)
        Spacer(modifier = Modifier.height(4.dp))
        Text(text = specialist.organization)
        Spacer(modifier = Modifier.height(20.dp))
        Text(text = "Контакты")
        Text(text = specialist.phone)
        Spacer(modifier = Modifier.height(4.dp))
        Text(text = specialist.email)
    }
}