package ru.walfi.nurturenook.feature_parent_child_section.presentation.child_section

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Badge
import androidx.compose.material.BadgedBox
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_comments.ParentCommentsScreen
import ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_info.ParentInfoScreen
import ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_schedule.ParentScheduleScreen
import ru.walfi.nurturenook.ui.theme.backgroundColor

@Composable
fun ParentChildSectionScreen(
    navController: NavController,
    viewModel: ParentChildSectionViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val navHostController = rememberNavController()
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is ParentChildSectionViewModel.UiEvent.OpenScreen -> {
                    navHostController.navigate(event.route)
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        bottomBar = {
            BottomNavigation(
                backgroundColor = backgroundColor
            ){
                state.bottomNavigationItemList.forEachIndexed { index, item ->
                    BottomNavigationItem(
                        selected = state.selectedItemIndex == index,
                        onClick = {
                            viewModel.onEvent(ParentChildSectionEvent.ItemChange(index, item.route))
                        },
                        label = {
                            Text(text = item.title)
                        },
                        alwaysShowLabel = false,
                        icon = {
                            BadgedBox(
                                badge = {
                                    if(item.badgeCount != null){
                                        Badge{
                                            Text(text = item.badgeCount.toString())
                                        }
                                    }
                                }
                            ) {
                                Icon(
                                    imageVector = if (index == state.selectedItemIndex) {
                                        item.selectedIcon
                                    } else item.unselectedIcon,
                                    contentDescription = item.title
                                )
                            }
                        }
                    )
                }
            }
        }
    ) {innerPadding ->
        NavHost(
            navController = navHostController,
            startDestination = Screen.ParentCommentsScreen.route,
            Modifier.padding(innerPadding)
        ){
            composable(route = Screen.ParentCommentsScreen.route){
                ParentCommentsScreen(navController = navController)
            }
            composable(route = Screen.ParentScheduleScreen.route){
                ParentScheduleScreen(navController = navController)
            }
            composable(route = Screen.ParentInfoScreen.route){
                ParentInfoScreen(navController = navController)
            }
        }
    }
}