package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_schedule

import ru.walfi.nurturenook.core.model.Task

sealed class ParentScheduleEvent {

    object PressedAddTaskButton: ParentScheduleEvent()
    object PressedCancelButton: ParentScheduleEvent()
    object PressedSaveButton: ParentScheduleEvent()

    class PressedDeleteButton(val task: Task): ParentScheduleEvent()

    class EnteredNewValue(val value: String): ParentScheduleEvent()
}