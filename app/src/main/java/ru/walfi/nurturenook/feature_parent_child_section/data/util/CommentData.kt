package ru.walfi.nurturenook.feature_parent_child_section.data.util

import androidx.room.ColumnInfo

data class CommentData(
    @ColumnInfo(name = "content") val content: String,
    @ColumnInfo(name = "lastName") val specialistLastName: String
)