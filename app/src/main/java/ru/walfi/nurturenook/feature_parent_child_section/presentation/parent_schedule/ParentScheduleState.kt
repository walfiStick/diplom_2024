package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_schedule

import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.core.util.TextFieldState

data class ParentScheduleState(
    val toolbarText: String = "Расписание",
    val tasksList: List<Task> = emptyList(),
    val currentTask: TextFieldState = TextFieldState(hint = "Введите задачу"),
    val isCreation: Boolean = false
)