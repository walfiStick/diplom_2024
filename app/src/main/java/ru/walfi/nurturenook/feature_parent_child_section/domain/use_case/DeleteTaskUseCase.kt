package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class DeleteTaskUseCase(
    private val repository: ParentChildSectionRepository
) {

    suspend operator fun invoke(task: Task){
        repository.deleteTask(task)
    }
}