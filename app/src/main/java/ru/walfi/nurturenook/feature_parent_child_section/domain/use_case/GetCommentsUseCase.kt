package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.feature_parent_child_section.data.util.CommentData
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class GetCommentsUseCase(
    private val repository: ParentChildSectionRepository
) {

    operator fun invoke(id: Int): Flow<List<CommentData>> {
        return repository.getCommentsByChildId(id)
    }
}