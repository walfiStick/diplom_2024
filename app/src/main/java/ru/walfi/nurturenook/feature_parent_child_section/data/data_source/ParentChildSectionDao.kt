package ru.walfi.nurturenook.feature_parent_child_section.data.data_source

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_parent_child_section.data.util.CommentData

@Dao
interface ParentChildSectionDao {

    @Query("SELECT comment.content, specialist.lastName FROM comment " +
            "JOIN specialist ON comment.specialistId = specialist.id " +
            "WHERE childId = :id")
    fun getCommentsByChildId(id: Int): Flow<List<CommentData>>

    @Query("SELECT * FROM specialist " +
            "JOIN specialisttochild ON specialist.id = specialisttochild.specialistId " +
            "WHERE specialisttochild.childCode = :childCode")
    fun getSpecialistsByChildCode(childCode: String): Flow<List<Specialist>>

    @Query("SELECT childCode FROM child WHERE id = :id")
    suspend fun getChildCodeByChildId(id: Int): String

    @Insert
    suspend fun insertTask(task: Task)

    @Query("SELECT * FROM task WHERE childId = :childId")
    fun getTasksByChildId(childId: Int): Flow<List<Task>>

    @Delete
    suspend fun deleteTask(task: Task)
}