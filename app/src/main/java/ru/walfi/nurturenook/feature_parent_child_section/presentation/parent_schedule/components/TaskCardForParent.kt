package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_schedule.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.ui.theme.commentCardsColor
import ru.walfi.nurturenook.ui.theme.finishedTaskCardsColor
import ru.walfi.nurturenook.ui.theme.secondTextColor
import ru.walfi.nurturenook.ui.theme.textColor
import ru.walfi.nurturenook.ui.theme.unFinishedTaskCardsColor

@Composable
fun TaskCardForParent(
    task: Task,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .clip(shape = RoundedCornerShape(12.dp))
            .fillMaxWidth()
            .background(if (task.executeFlag) finishedTaskCardsColor else unFinishedTaskCardsColor)
            .padding(start = 30.dp, end = 8.dp, top = 8.dp, bottom = 8.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = task.description,
            style = TextStyle(
                color = if(task.executeFlag) secondTextColor else textColor
            )
        )
        IconButton(onClick = onClick) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = null
            )
        }
    }
}