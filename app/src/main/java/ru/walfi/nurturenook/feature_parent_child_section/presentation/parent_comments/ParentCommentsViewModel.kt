package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_comments

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_parent_child_section.domain.use_case.ParentChildSectionUseCases
import javax.inject.Inject

@HiltViewModel
class ParentCommentsViewModel @Inject constructor(
    private val parentChildSectionUseCases: ParentChildSectionUseCases
): ViewModel() {

    private val _state = mutableStateOf(ParentCommentsState())
    val state = _state

    private var childId: Int? = null

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        viewModelScope.launch {
            childId = parentChildSectionUseCases.getChildIdParentUseCase()
        }
        childId?.let {
            parentChildSectionUseCases.getCommentsUseCase(it).onEach { comments ->
                _state.value = state.value.copy(commentList = comments)
            }.launchIn(viewModelScope)
        }
    }

    fun onEvent(event: ParentCommentsEvent){
        when(event){
            is ParentCommentsEvent.PressedBackButton ->{
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
        }
    }

    sealed class UiEvent{
        object NavigateUp: UiEvent()
    }
}