package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

import ru.walfi.nurturenook.core.model.InvalidTaskException
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class AddTaskUseCase(
    private val repository: ParentChildSectionRepository
) {
    @Throws
    suspend operator fun invoke(task: Task){
        if(task.description.isBlank()){
            throw InvalidTaskException("Задча не может быть пустой")
        }
        repository.insertTask(task)
    }
}