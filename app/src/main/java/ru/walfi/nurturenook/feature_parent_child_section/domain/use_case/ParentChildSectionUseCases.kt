package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

data class ParentChildSectionUseCases(
    val getCommentsUseCase: GetCommentsUseCase,
    val getChildIdParentUseCase: GetChildIdParentUseCase,
    val getSpecialistsUseCase: GetSpecialistsUseCase,
    val getTasksUseCase: GetTasksUseCase,
    val addTaskUseCase: AddTaskUseCase,
    val deleteTaskUseCase: DeleteTaskUseCase
)
