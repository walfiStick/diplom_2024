package ru.walfi.nurturenook.feature_parent_child_section.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_parent_child_section.data.data_source.ParentChildSectionDao
import ru.walfi.nurturenook.feature_parent_child_section.data.util.CommentData
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class ParentChildSectionRepositoryImpl(
    private val dao: ParentChildSectionDao,
    private val dataStoreManager: DataStoreManager
): ParentChildSectionRepository {

    override fun getCommentsByChildId(id: Int): Flow<List<CommentData>> {
        return dao.getCommentsByChildId(id)
    }

    override suspend fun getChildId(): Int {
        return dataStoreManager.getChildId().first()
    }

    override fun getSpecialistsByChildCode(childCode: String): Flow<List<Specialist>> {
        return dao.getSpecialistsByChildCode(childCode)
    }

    override suspend fun getChildCodeByChildId(id: Int): String {
        return dao.getChildCodeByChildId(id)
    }

    override suspend fun insertTask(task: Task) {
        dao.insertTask(task)
    }

    override fun getTasksByChildId(childId: Int): Flow<List<Task>> {
        return dao.getTasksByChildId(childId)
    }

    override suspend fun deleteTask(task: Task) {
        dao.deleteTask(task)
    }
}