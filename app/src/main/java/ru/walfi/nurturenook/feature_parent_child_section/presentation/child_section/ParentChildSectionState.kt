package ru.walfi.nurturenook.feature_parent_child_section.presentation.child_section

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.EventNote
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Groups
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.outlined.EventNote
import androidx.compose.material.icons.outlined.Face
import androidx.compose.material.icons.outlined.Groups
import androidx.compose.material.icons.outlined.Mail
import ru.walfi.nurturenook.core.util.BottomNavigationItem
import ru.walfi.nurturenook.core.util.Screen

data class ParentChildSectionState(
    val selectedItemIndex: Int = 0,
    val bottomNavigationItemList: List<BottomNavigationItem> = listOf(
        BottomNavigationItem(
            title = "Комментарии",
            selectedIcon = Icons.Filled.Mail,
            unselectedIcon = Icons.Outlined.Mail,
            route = Screen.ParentCommentsScreen.route
        ),
        BottomNavigationItem(
            title = "Расписание",
            selectedIcon = Icons.Filled.EventNote,
            unselectedIcon = Icons.Outlined.EventNote,
            route = Screen.ParentScheduleScreen.route
        ),
        BottomNavigationItem(
            title = "Специалисты",
            selectedIcon = Icons.Filled.Groups,
            unselectedIcon = Icons.Outlined.Groups,
            route = Screen.ParentInfoScreen.route
        )
    )
)
