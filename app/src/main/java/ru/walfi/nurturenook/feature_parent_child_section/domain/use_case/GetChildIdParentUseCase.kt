package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class GetChildIdParentUseCase(
    private val repository: ParentChildSectionRepository
) {

    suspend operator fun invoke(): Int = repository.getChildId()
}