package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_comments

sealed class ParentCommentsEvent{

    object PressedBackButton: ParentCommentsEvent()
}
