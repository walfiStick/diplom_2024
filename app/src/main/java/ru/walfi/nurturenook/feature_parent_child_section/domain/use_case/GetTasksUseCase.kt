package ru.walfi.nurturenook.feature_parent_child_section.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_parent_child_section.domain.repository.ParentChildSectionRepository

class GetTasksUseCase(
    private val repository: ParentChildSectionRepository
) {

    operator fun invoke(childId: Int): Flow<List<Task>>{
        return repository.getTasksByChildId(childId)
    }
}