package ru.walfi.nurturenook.feature_parent_child_section.presentation.parent_info

import ru.walfi.nurturenook.core.model.Specialist

data class ParentInfoState(
    val toolbarText: String = "Специалисты",
    val specialistsList: List<Specialist> = emptyList()
)