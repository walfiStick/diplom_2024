package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

data class SpecialistChildSectionUseCases(
    val getChildUseCase: GetChildUseCase,
    val getChildIdUseCase: GetChildIdUseCase,
    val logOutChildUseCase: LogOutChildUseCase,
    val addCommentUseCase: AddCommentUseCase,
    val getCommentByChildAndSpecialistIdUseCase: GetCommentByChildAndSpecialistIdUseCase,
    val getSpecialistIdUseCase: GetSpecialistIdUseCase,
    val deleteCommentUseCase: DeleteCommentUseCase,
    val addPurposeUseCase: AddPurposeUseCase,
    val getPurposesByChildIdUseCase: GetPurposesByChildIdUseCase
)
