package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class GetChildUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    suspend operator fun invoke(id: Int): Child {
        return repository.getChildById(id)
    }
}