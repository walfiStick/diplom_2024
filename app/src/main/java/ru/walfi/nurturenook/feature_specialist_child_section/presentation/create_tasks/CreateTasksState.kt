package ru.walfi.nurturenook.feature_specialist_child_section.presentation.create_tasks

import ru.walfi.nurturenook.core.util.Screen

class CreateTasksState(
    val toolbarText: String = "Создать уровень",
    val gameList: List<ChooseGameForCreateLevel> = listOf(
        ChooseGameForCreateLevel(
            name = "Изучение слов",
            screen = Screen.CreateWordLvlScreen
        )
    )
)

data class ChooseGameForCreateLevel(
    val name: String,
    val screen: Screen
)