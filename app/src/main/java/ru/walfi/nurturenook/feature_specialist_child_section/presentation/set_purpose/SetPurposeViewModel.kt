package ru.walfi.nurturenook.feature_specialist_child_section.presentation.set_purpose

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.SpecialistChildSectionUseCases
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.util.PurposeInfo
import javax.inject.Inject

@HiltViewModel
class SetPurposeViewModel @Inject constructor(
    private val specialistChildSectionUseCases: SpecialistChildSectionUseCases
) : ViewModel() {

    private val _state = mutableStateOf(
        SetPurposeState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var childId: Int = -1
    private var isAllDone = false
    private var copylearningWordsGame: PurposeInfo? = null
//    private var copylearningActionsGame: PurposeInfo? = null

    init {
        viewModelScope.launch {
            childId = specialistChildSectionUseCases.getChildIdUseCase()
            childId.let {
                specialistChildSectionUseCases.getPurposesByChildIdUseCase(it).onEach { purposesList ->
                    purposesList.onEach {purpose ->
                        when (purpose.gameName) {
                            "Изучение слов" -> {
                                _state.value = state.value.copy(
                                    learningWordsGame = PurposeInfo(
                                        name = purpose.gameName,
                                        targetValue = purpose.targetValue,
                                        completedValue = purpose.completedValue
                                    )
                                )
                            }
//                            "Изучение действий" -> {
//                                _state.value = state.value.copy(
//                                    learningActionsGame = PurposeInfo(
//                                        name = purpose.gameName,
//                                        targetValue = purpose.targetValue,
//                                        completedValue = purpose.completedValue
//                                    )
//                                )
//                            }
                        }
                        if(purpose.completedValue != purpose.targetValue || purpose.targetValue == 0){
                            isAllDone = false
                        }
                    }
                    if(isAllDone){
                        _state.value = state.value.copy(
                            purposesState = PurposesState.IsAllDone
                        )
                    }
                }.launchIn(viewModelScope)
            }
        }
    }

    fun onEvent(event: SetPurposeEvent) {
        when (event) {
            is SetPurposeEvent.PressedEditButton -> {
                copylearningWordsGame = _state.value.learningWordsGame
//                copylearningActionsGame = _state.value.learningActionsGame
                _state.value = state.value.copy(
                    purposesState = PurposesState.IsEditing
                )
            }
            is SetPurposeEvent.PressedSaveChangeButton -> {
                viewModelScope.launch {
                    specialistChildSectionUseCases.addPurposeUseCase(
                        Purpose(
                            gameName = _state.value.learningWordsGame.name,
                            childId = childId,
                            targetValue = _state.value.learningWordsGame.targetValue,
                            completedValue = 0
                        )
                    )
//                    specialistChildSectionUseCases.addPurposeUseCase(
//                        Purpose(
//                            gameName = _state.value.learningActionsGame.name,
//                            childId = childId,
//                            targetValue = _state.value.learningActionsGame.targetValue,
//                            completedValue = 0
//                        )
//                    )
                }
                _state.value = state.value.copy(
                    purposesState = PurposesState.IsSet
                )
            }
            is SetPurposeEvent.EnteredTargetValue -> {
                when (event.name) {
                    "Изучение слов" -> {
                        _state.value = state.value.copy(
                            learningWordsGame = PurposeInfo(
                                name = "Изучение слов",
                                targetValue = try {
                                    event.value.toInt()
                                } catch (e: Exception) {
                                    0
                                },
                                completedValue = 0
                            )
                        )
                    }
//                    "Изучение действий" -> {
//                        _state.value = state.value.copy(
//                            learningActionsGame = PurposeInfo(
//                                name = "Изучение действий",
//                                targetValue = try {
//                                    event.value.toInt()
//                                } catch (e: Exception) {
//                                    0
//                                },
//                                completedValue = 0
//                            )
//                        )
//                    }
                }
            }
            is SetPurposeEvent.PressedConfirmButton -> {
                //to do
            }
            is SetPurposeEvent.PressedCancelButton -> {
                _state.value = state.value.copy(
                    purposesState = PurposesState.IsSet,
                    learningWordsGame = copylearningWordsGame!!,
//                    learningActionsGame = copylearningActionsGame!!
                )
            }
        }
    }

    sealed class UiEvent {
        data class ShowSnackbar(val message: String) : UiEvent()
    }
}