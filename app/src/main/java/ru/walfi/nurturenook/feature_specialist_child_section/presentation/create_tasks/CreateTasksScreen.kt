package ru.walfi.nurturenook.feature_specialist_child_section.presentation.create_tasks

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Badge
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import ru.walfi.nurturenook.core.util.BaseClickableTextRow
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.set_purpose.TestPurposeRow
import ru.walfi.nurturenook.ui.theme.backBasicTextColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateTasksScreen(
    navController: NavController,
    viewModel: CreateTasksViewModel = hiltViewModel()
) {
    val state = viewModel.state
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                )
            )
        }
    ) {
        LazyColumn(modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp)
        ){
            items(state.gameList){
                Spacer(modifier = Modifier.height(4.dp))
                BaseClickableTextRow(
                    text = it.name,
                    onClick = {navController.navigate(it.screen.route)}
                )
                Spacer(modifier = Modifier.height(4.dp))
                //для теста
                TestTextRow(text = "Вычисления")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Эмоции")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Загадки")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Изучение дейсвий")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Животные")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Арифметика")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Фигуры")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Лиса Алиса")
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(text = "Почемучка")
            }
        }
    }
}

//для диплома
@Composable
fun TestTextRow(
    text: String,
    purpose: Int = 0
){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(8.dp))
            .background(backBasicTextColor)
            .padding(vertical = 8.dp, horizontal = 6.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = text,
            style = TextStyle(
                fontSize = 16.sp
            )
        )
        if(purpose != 0){
            Badge { Text(text = purpose.toString())}
        }
    }
}

