package ru.walfi.nurturenook.feature_specialist_child_section.data.data_source

import android.database.sqlite.SQLiteException
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.Purpose

@Dao
interface SpecialistChildSectionDao {

    @Query("SELECT * FROM child WHERE id = :id")
    @Throws(SQLiteException::class)
    suspend fun getChildById(id: Int): Child

    @Insert
    suspend fun insertComment(comment: Comment)

    @Query("SELECT * FROM comment WHERE (childId = :childId) AND (specialistId = :specialistId)")
    fun getCommentsByChildAndSpecialistId(childId: Int, specialistId: Int): Flow<List<Comment>>

    @Delete
    suspend fun deleteComment(comment: Comment)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPurpose(purpose: Purpose)

    @Query("SELECT * FROM purpose WHERE childId = :id")
    fun getPurposesByChildId(id: Int): Flow<List<Purpose>>

}