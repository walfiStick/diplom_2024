package ru.walfi.nurturenook.feature_specialist_child_section.presentation.specialist_comments

import ru.walfi.nurturenook.core.model.Comment

sealed class SpecialistCommentsEvent{

    class EnteredComment(val value: String): SpecialistCommentsEvent()

    object PressedSendButton: SpecialistCommentsEvent()

    class PressedDeleteButton(val comment: Comment): SpecialistCommentsEvent()
}
