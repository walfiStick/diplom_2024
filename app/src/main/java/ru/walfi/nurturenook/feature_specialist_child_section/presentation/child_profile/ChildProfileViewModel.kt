package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_profile

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.SpecialistChildSectionUseCases
import javax.inject.Inject

@HiltViewModel
class ChildProfileViewModel @Inject constructor(
    private val specialistChildSectionUseCases: SpecialistChildSectionUseCases
): ViewModel() {

    private val _state = mutableStateOf(
        ChildProfileState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var childId: Int = -1

    init {
        viewModelScope.launch {
            childId = specialistChildSectionUseCases.getChildIdUseCase()
            specialistChildSectionUseCases.getChildUseCase(childId).let {
                _state.value = state.value.copy(
                    child = Child(
                        lastName = it.lastName,
                        firstName = it.firstName,
                        patronymic = it.patronymic,
                        birthday = it.birthday,
                        disease = it.disease,
                        parentLastName = it.parentLastName,
                        parentFirstName = it.parentFirstName,
                        parentPatronymic = it.parentPatronymic,
                        parentPhone = it.parentPhone,
                        childCode = it.childCode,
                        id = childId
                    )
                )
            }
        }
    }

    fun onEvent(event: ChildProfileEvent){
        when(event){
            is ChildProfileEvent.PressedBackButton -> {
                viewModelScope.launch {
                    specialistChildSectionUseCases.logOutChildUseCase
                    _eventFlow.emit(UiEvent.NavigationUp)
                }
            }
            is ChildProfileEvent.PressedEditButton -> {}
        }
    }


    sealed class UiEvent{
        object NavigationUp: UiEvent()
        class OpenScreen(val route: String): UiEvent()
        data class ShowSnackbar(val message: String): UiEvent()
    }
}