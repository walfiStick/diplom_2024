package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_profile

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.BaseInfoRow
import ru.walfi.nurturenook.core.util.InfoRowState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChildProfileScreen(
    navController: NavController,
    viewModel: ChildProfileViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val items = listOf(
        InfoRowState(name = "Фамилия", text = state.child.lastName),
        InfoRowState(name = "Имя", text = state.child.firstName),
        InfoRowState(name = "Отчество", text = state.child.patronymic),
        InfoRowState(name = "Дата рождения", text = state.child.birthday),
        InfoRowState(name = "Заболевание", text = state.child.disease),
        InfoRowState(name = "Уникальный код", text = state.child.childCode),
        //parent
        InfoRowState(name = "Фамилия", text = state.child.parentLastName),
        InfoRowState(name = "Имя", text = state.child.parentFirstName),
        InfoRowState(name = "Отчество", text = state.child.parentPatronymic),
        InfoRowState(name = "Номер", text = state.child.parentPhone),
    )

    LaunchedEffect(key1 = true){
        viewModel.eventFlow.collectLatest { event ->
            when(event){
                is ChildProfileViewModel.UiEvent.NavigationUp ->{
                    navController.navigateUp()
                }
                is ChildProfileViewModel.UiEvent.ShowSnackbar ->{
                    scaffoldState.snackbarHostState.showSnackbar(message = event.message)
                }
                is ChildProfileViewModel.UiEvent.OpenScreen ->{}
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                navigationIcon = {
                    IconButton(onClick = { viewModel.onEvent(ChildProfileEvent.PressedBackButton) }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Выйти"
                        )
                    }
                },
                actions = {
                    IconButton(onClick = { /*add edit profile*/ }) {
                        Icon(
                            imageVector = Icons.Default.Edit,
                            contentDescription = "Редактировать профиль"
                        )
                    }
                }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ){
            items(items){
                Spacer(modifier = Modifier.height(8.dp))
                BaseInfoRow(infoRowState = it)
                Spacer(modifier = Modifier.height(8.dp))
                if(it.text == state.child.childCode){
                    Spacer(modifier = Modifier.height(16.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ){
                        Text(
                            text = "Родитель",
                            fontSize = 18.sp,
                        )
                    }
                }
            }
        }
    }
}