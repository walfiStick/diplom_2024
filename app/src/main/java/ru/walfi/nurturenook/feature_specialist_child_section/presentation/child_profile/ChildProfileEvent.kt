package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_profile

sealed class ChildProfileEvent{

    object PressedEditButton: ChildProfileEvent()

    object PressedBackButton: ChildProfileEvent()
}
