package ru.walfi.nurturenook.feature_specialist_child_section.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.Purpose

interface SpecialistChildSectionRepository {

    suspend fun getChildById(id: Int): Child

    suspend fun getChildId(): Int

    suspend fun logOutChild()

    suspend fun insertComment(comment: Comment)

    fun getCommentsByChildAndSpecialistId(childId: Int, specialistId: Int): Flow<List<Comment>>

    suspend fun getSpecialistId(): Int

    suspend fun deleteComment(comment: Comment)

    suspend fun insertPurpose(purpose: Purpose)

    fun getPurposesByChildId(id: Int): Flow<List<Purpose>>
}