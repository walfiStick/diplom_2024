package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_profile

import ru.walfi.nurturenook.core.model.Child

data class ChildProfileState(
    val toolbarText: String = "Профиль ребенка",
    val child: Child = Child(
        lastName = "",
        firstName = "",
        patronymic = "",
        birthday = "",
        disease = "",
        parentLastName = "",
        parentFirstName = "",
        parentPatronymic = "",
        parentPhone = "",
        childCode = ""
    )
)
