package ru.walfi.nurturenook.feature_specialist_child_section.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_specialist_child_section.data.data_source.SpecialistChildSectionDao
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class SpecialistChildSectionRepositoryImpl(
    private val dao: SpecialistChildSectionDao,
    private val dataStoreManager: DataStoreManager
) : SpecialistChildSectionRepository {

    override suspend fun getChildById(id: Int): Child {
        return dao.getChildById(id)
    }

    override suspend fun getChildId(): Int {
        return dataStoreManager.getChildId().first()
    }

    override suspend fun logOutChild() {
        dataStoreManager.saveChildId(-1)
    }

    override suspend fun insertComment(comment: Comment) {
        dao.insertComment(comment)
    }

    override fun getCommentsByChildAndSpecialistId(childId: Int, specialistId: Int): Flow<List<Comment>> {
        return dao.getCommentsByChildAndSpecialistId(childId =  childId, specialistId = specialistId)
    }

    override suspend fun getSpecialistId(): Int {
        return dataStoreManager.getAccountData().first().accountId
    }

    override suspend fun deleteComment(comment: Comment) {
        dao.deleteComment(comment)
    }

    override suspend fun insertPurpose(purpose: Purpose) {
        dao.insertPurpose(purpose)
    }

    override fun getPurposesByChildId(id: Int): Flow<List<Purpose>> {
        return dao.getPurposesByChildId(id)
    }
}