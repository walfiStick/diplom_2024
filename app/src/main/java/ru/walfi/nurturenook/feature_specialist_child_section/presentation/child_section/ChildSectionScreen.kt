package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_section

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Badge
import androidx.compose.material.BadgedBox
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_profile.ChildProfileScreen
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.create_tasks.CreateTasksScreen
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.set_purpose.SetPurposeScreen
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.specialist_comments.SpecialistCommentsScreen
import ru.walfi.nurturenook.ui.theme.backgroundColor

@Composable
fun ChildSectionScreen(
    navController: NavController,
    childId: Int?,
    viewModel: ChildSectionViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val navHostController = rememberNavController()
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is ChildSectionViewModel.UiEvent.OpenScreen -> {
                    navHostController.navigate(event.route + "childId=${childId}")
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        bottomBar = {
            BottomNavigation(
                backgroundColor = backgroundColor
            ){
                state.bottomNavigationItemList.forEachIndexed { index, item ->
                    BottomNavigationItem(
                        selected = state.selectedItemIndex == index,
                        onClick = {
                            viewModel.onEvent(ChildSectionEvent.ItemChange(index, item.route))
                        },
                        label = {
                            Text(text = item.title)
                        },
                        alwaysShowLabel = false,
                        icon = {
                            BadgedBox(
                                badge = {
                                    if(item.badgeCount != null){
                                        Badge{
                                            Text(text = item.badgeCount.toString())
                                        }
                                    }
                                }
                            ) {
                                Icon(
                                    imageVector = if (index == state.selectedItemIndex) {
                                        item.selectedIcon
                                    } else item.unselectedIcon,
                                    contentDescription = item.title
                                )
                            }
                        }
                    )
                }
            }
        }
    ) {innerPadding ->
        NavHost(
            navController = navHostController,
            startDestination = Screen.SpecialistChildProfileScreen.route + "childId={childId}",
            Modifier.padding(innerPadding)
        ){
            composable(
                route = Screen.SpecialistChildProfileScreen.route + "childId={childId}",
                arguments = listOf(
                    navArgument("childId"){ type = NavType.IntType }
                )
            ){
                ChildProfileScreen(navController = navController)
            }
            composable(
                route = Screen.SpecialistCommentsScreen.route + "childId={childId}",
                arguments = listOf(
                    navArgument("childId"){ type = NavType.IntType }
                )
            ){
                SpecialistCommentsScreen(navController = navController)
            }
            composable(
                route = Screen.SpecialistSetPurposeScreen.route + "childId={childId}",
                arguments = listOf(
                    navArgument("childId"){ type = NavType.IntType }
                )
            ){
                SetPurposeScreen(navController = navController)
            }
            composable(
                route = Screen.SpecialistCreateTasksScreen.route + "childId={childId}",
                arguments = listOf(
                    navArgument("childId"){ type = NavType.IntType }
                )
            ){
                CreateTasksScreen(navController = navController)
            }
        }
    }
}