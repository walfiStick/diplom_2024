package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.InvalidCommentException
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class AddCommentUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    @Throws(InvalidCommentException::class)
    suspend operator fun invoke(comment: Comment){
        if(comment.content.isBlank()){
            throw InvalidCommentException("Комментарий не может быть пустым")
        }
        repository.insertComment(comment)
    }
}