package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class GetCommentByChildAndSpecialistIdUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    operator fun invoke(childId: Int, specialistId: Int): Flow<List<Comment>> {
        return repository.getCommentsByChildAndSpecialistId(childId = childId, specialistId = specialistId)
    }
}