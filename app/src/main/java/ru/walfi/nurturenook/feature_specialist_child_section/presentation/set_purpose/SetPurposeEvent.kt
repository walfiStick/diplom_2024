package ru.walfi.nurturenook.feature_specialist_child_section.presentation.set_purpose

sealed class SetPurposeEvent{

    class EnteredTargetValue(val value: String, val name: String): SetPurposeEvent()

    object PressedConfirmButton: SetPurposeEvent()

    object PressedEditButton: SetPurposeEvent()
    object PressedCancelButton: SetPurposeEvent()
    object PressedSaveChangeButton: SetPurposeEvent()
}
