package ru.walfi.nurturenook.feature_specialist_child_section.presentation.specialist_comments

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.BaseCommentCardForParent
import ru.walfi.nurturenook.core.util.BaseCommentCardForSpecialist
import ru.walfi.nurturenook.ui.theme.backgroundColor
import ru.walfi.nurturenook.ui.theme.borderTextFieldColor
import ru.walfi.nurturenook.ui.theme.contentTextFieldColor
import ru.walfi.nurturenook.ui.theme.hintTextFieldColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SpecialistCommentsScreen(
    navController: NavController,
    viewModel: SpecialistCommentsViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is SpecialistCommentsViewModel.UiEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(message = event.message)
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        modifier = Modifier.background(backgroundColor),
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
            )
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            LazyColumn(
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 16.dp)
            ) {
                items(state.commentsList) {
                    Spacer(modifier = Modifier.height(4.dp))
                    BaseCommentCardForSpecialist(
                        content = it.content,
                        onClick = {viewModel.onEvent(SpecialistCommentsEvent.PressedDeleteButton(it))}
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                }
            }
            Spacer(modifier = Modifier.height(8.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                TextField(
                    value = state.commentsField.text,
                    onValueChange = { viewModel.onEvent(SpecialistCommentsEvent.EnteredComment(it)) },
                    modifier = Modifier
                        .height(100.dp)
                        .weight(1f)
                        .clip(RoundedCornerShape(8.dp))
                        .border(
                            color = borderTextFieldColor,
                            shape = RoundedCornerShape(8.dp),
                            width = 2.dp
                        ),
                    placeholder = { Text(text = state.commentsField.hint, color = hintTextFieldColor) },
                    textStyle = TextStyle(
                        color = contentTextFieldColor,
                        fontSize = 16.sp
                    ),
                    colors = TextFieldDefaults.textFieldColors(
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        cursorColor = hintTextFieldColor,
                        backgroundColor = backgroundColor.copy(0f)
                    )
                )
                Spacer(modifier = Modifier.width(8.dp))
                IconButton(onClick = { viewModel.onEvent(SpecialistCommentsEvent.PressedSendButton) }) {
                    Icon(
                        imageVector = Icons.Default.Send,
                        contentDescription = "Отправить"
                    )
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}