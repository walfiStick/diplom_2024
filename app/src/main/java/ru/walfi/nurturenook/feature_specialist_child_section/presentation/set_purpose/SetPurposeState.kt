package ru.walfi.nurturenook.feature_specialist_child_section.presentation.set_purpose

import ru.walfi.nurturenook.feature_specialist_child_section.presentation.util.PurposeInfo

data class SetPurposeState(
    val toolbarText: String = "Поставить цель",
    val purposesState: PurposesState = PurposesState.IsSet,
    val learningWordsGame: PurposeInfo = PurposeInfo(
        name = "Изучение слов",
        targetValue = 0,
        completedValue = 0
    ),
//    val learningActionsGame: PurposeInfo = PurposeInfo(
//        name = "Изучение действий",
//        targetValue = 0,
//        completedValue = 0
//    ),
)

enum class PurposesState{
    IsSet,
    IsAllDone,
    IsEditing
}
