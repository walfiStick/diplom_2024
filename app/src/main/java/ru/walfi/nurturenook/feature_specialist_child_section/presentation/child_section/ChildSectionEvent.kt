package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_section

sealed class ChildSectionEvent{

    class ItemChange(val index: Int, val route: String): ChildSectionEvent()
}
