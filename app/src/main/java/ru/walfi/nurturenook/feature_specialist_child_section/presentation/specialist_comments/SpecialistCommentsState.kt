package ru.walfi.nurturenook.feature_specialist_child_section.presentation.specialist_comments

import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.util.TextFieldState

data class SpecialistCommentsState(
    val toolbarText: String = "Комментарии",
    val commentsList: List<Comment> = emptyList(),
    val commentsField: TextFieldState = TextFieldState(hint = "Введите комментарий")
)
