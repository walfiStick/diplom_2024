package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class GetChildIdUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    suspend operator fun invoke(): Int{
        return repository.getChildId()
    }
}