package ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_section

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddTask
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.FlagCircle
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.outlined.AddTask
import androidx.compose.material.icons.outlined.Face
import androidx.compose.material.icons.outlined.FlagCircle
import androidx.compose.material.icons.outlined.Mail
import ru.walfi.nurturenook.core.util.BottomNavigationItem
import ru.walfi.nurturenook.core.util.Screen

data class ChildSectionState(
    val bottomNavigationItemList: List<BottomNavigationItem> = listOf(
        BottomNavigationItem(
            title = "Профиль",
            selectedIcon = Icons.Filled.Face,
            unselectedIcon = Icons.Outlined.Face,
            route = Screen.SpecialistChildProfileScreen.route
        ),
        BottomNavigationItem(
            title = "Комментарии",
            selectedIcon = Icons.Filled.Mail,
            unselectedIcon = Icons.Outlined.Mail,
            route = Screen.SpecialistCommentsScreen.route
        ),
        BottomNavigationItem(
            title = "Цели",
            selectedIcon = Icons.Filled.FlagCircle,
            unselectedIcon = Icons.Outlined.FlagCircle,
            route = Screen.SpecialistSetPurposeScreen.route
        ),
        BottomNavigationItem(
            title = "Задачи",
            selectedIcon = Icons.Filled.AddTask,
            unselectedIcon = Icons.Outlined.AddTask,
            route = Screen.SpecialistCreateTasksScreen.route
        ),
    ),
    val selectedItemIndex: Int = 0
)
