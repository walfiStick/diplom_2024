package ru.walfi.nurturenook.feature_specialist_child_section.presentation.specialist_comments

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.InvalidCommentException
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case.SpecialistChildSectionUseCases
import javax.inject.Inject

@HiltViewModel
class SpecialistCommentsViewModel @Inject constructor(
    private val specialistChildSectionUseCases: SpecialistChildSectionUseCases
) : ViewModel() {

    private val _state = mutableStateOf(
        SpecialistCommentsState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var childId = -1
    private var specialistId = -1

    init {
        viewModelScope.launch {
            childId = specialistChildSectionUseCases.getChildIdUseCase()
            specialistId = specialistChildSectionUseCases.getSpecialistIdUseCase()
        }
        specialistChildSectionUseCases.getCommentByChildAndSpecialistIdUseCase(
            childId = childId,
            specialistId = specialistId
        ).onEach {
            _state.value = state.value.copy(
                commentsList = it
            )
        }.launchIn(viewModelScope)
    }

    fun onEvent(event: SpecialistCommentsEvent) {
        when (event) {
            is SpecialistCommentsEvent.EnteredComment -> {
                _state.value = state.value.copy(
                    commentsField = TextFieldState(hint = "Введите комментарий", text = event.value)
                )
            }
            is SpecialistCommentsEvent.PressedSendButton -> {
                viewModelScope.launch {
                    try{
                        specialistChildSectionUseCases.addCommentUseCase(
                            Comment(
                                content = _state.value.commentsField.text,
                                childId = childId,
                                specialistId = specialistId
                            )
                        )
                        _state.value = state.value.copy(
                            commentsField = TextFieldState(hint = "Введите комментарий")
                        )
                    }catch (e: InvalidCommentException){
                        viewModelScope.launch {
                            _eventFlow.emit(UiEvent.ShowSnackbar(e.message ?: "Что-то пошло не так"))
                        }
                    }
                }
            }
            is SpecialistCommentsEvent.PressedDeleteButton -> {
                viewModelScope.launch {
                    specialistChildSectionUseCases.deleteCommentUseCase(event.comment)
                }
            }
        }
    }

    sealed class UiEvent {
        data class ShowSnackbar(val message: String) : UiEvent()
    }
}