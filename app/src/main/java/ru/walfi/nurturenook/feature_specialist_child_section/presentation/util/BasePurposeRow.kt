package ru.walfi.nurturenook.feature_specialist_child_section.presentation.util

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ru.walfi.nurturenook.ui.theme.backBasicTextColor
import ru.walfi.nurturenook.ui.theme.childCardsColor

@Composable
fun BasePurposeRow(
    purposeInfo: PurposeInfo,
    readOnly: Boolean = true,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(Color.Transparent)
            .padding(horizontal = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = purposeInfo.name,
            fontSize = 16.sp
        )
        if(readOnly){
            Text(
                text = if (purposeInfo.targetValue == 0) "--"
                        else "${purposeInfo.completedValue}/${purposeInfo.targetValue}",
                style = TextStyle(
                    background = if (purposeInfo.completedValue == purposeInfo.targetValue && purposeInfo.targetValue != 0) childCardsColor
                                    else Color.Transparent,
                    fontSize = 16.sp
                ),
                modifier = Modifier
                    .padding(8.dp)
                    .width(50.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
        }else{
            BasicTextField(
                value = if (purposeInfo.targetValue == 0) "" else purposeInfo.targetValue.toString(),
                onValueChange = onValueChange,
                textStyle = TextStyle(
                    fontSize = 16.sp
                ),
                modifier = Modifier
                    .width(50.dp)
                    .padding(8.dp)
                    .clip(RoundedCornerShape(6.dp))
                    .background(backBasicTextColor),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
            )
        }
    }
}