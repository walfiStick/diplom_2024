package ru.walfi.nurturenook.feature_specialist_child_section.presentation.util

data class PurposeInfo(
    val name: String,
    val targetValue: Int,
    val completedValue: Int
)
