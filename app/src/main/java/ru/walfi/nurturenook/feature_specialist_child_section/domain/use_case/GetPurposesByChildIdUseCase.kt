package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class GetPurposesByChildIdUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    operator fun invoke(id: Int): Flow<List<Purpose>>{
        return repository.getPurposesByChildId(id)
    }
}