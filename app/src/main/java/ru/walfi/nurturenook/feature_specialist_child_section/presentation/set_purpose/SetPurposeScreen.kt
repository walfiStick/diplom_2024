@file:OptIn(ExperimentalMaterial3Api::class)

package ru.walfi.nurturenook.feature_specialist_child_section.presentation.set_purpose

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.DoneAll
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.util.BasePurposeRow
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.util.PurposeInfo
import ru.walfi.nurturenook.ui.theme.backBasicTextColor
import ru.walfi.nurturenook.ui.theme.backgroundColor
import ru.walfi.nurturenook.ui.theme.childCardsColor

@Composable
fun SetPurposeScreen(
    navController: NavController,
    viewModel: SetPurposeViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val items = listOf(
        state.learningWordsGame,
//        state.learningActionsGame
    )

    Scaffold(
        scaffoldState = scaffoldState,
        modifier = Modifier.background(backgroundColor),
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                actions = {
                    AnimatedVisibility(visible = state.purposesState == PurposesState.IsSet) {
                        IconButton(onClick = { viewModel.onEvent(SetPurposeEvent.PressedEditButton) }) {
                            Icon(
                                imageVector = Icons.Default.Edit,
                                contentDescription = "Редактировать"
                            )
                        }
                    }
                    AnimatedVisibility(visible = state.purposesState == PurposesState.IsAllDone) {
                        IconButton(onClick = { viewModel.onEvent(SetPurposeEvent.PressedConfirmButton) }) {
                            Icon(
                                imageVector = Icons.Default.DoneAll,
                                contentDescription = "Принять"
                            )
                        }
                    }
                    AnimatedVisibility(visible = state.purposesState == PurposesState.IsEditing) {
                        Row{
                            IconButton(onClick = { viewModel.onEvent(SetPurposeEvent.PressedCancelButton) }) {
                                Icon(
                                    imageVector = Icons.Default.Close,
                                    contentDescription = "Отменить"
                                )
                            }
                            IconButton(onClick = { viewModel.onEvent(SetPurposeEvent.PressedSaveChangeButton) }) {
                                Icon(
                                    imageVector = Icons.Default.Check,
                                    contentDescription = "Сохранить"
                                )
                            }
                        }
                    }
                }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ) {
            items(items) { purposeInfo ->
                BasePurposeRow(
                    purposeInfo = purposeInfo,
                    readOnly = state.purposesState != PurposesState.IsEditing,
                    onValueChange = { viewModel.onEvent(SetPurposeEvent.EnteredTargetValue(it, purposeInfo.name)) })
                //для теста
                TestPurposeRow(name = "Вычисления", targetValue = 7, completedValue = 5)
                TestPurposeRow(name = "Эмоции", targetValue = 3, completedValue = 1)
                TestPurposeRow(name = "Изучение дейсвий", targetValue = 3, completedValue = 3)
                TestPurposeRow(name = "Загадки", targetValue = 1, completedValue = 1)
                TestPurposeRow(name = "Животные", targetValue = 0, completedValue = 0)
                TestPurposeRow(name = "Арифметика", targetValue = 2, completedValue = 1)
                TestPurposeRow(name = "Фигуры", targetValue = 1, completedValue = 1)
                TestPurposeRow(name = "Лиса Алиса", targetValue = 2, completedValue = 0)
                TestPurposeRow(name = "Почемучка", targetValue = 0, completedValue = 0)
            }


        }
    }
}


//для диплома
@Composable
fun TestPurposeRow(
    name: String,
    targetValue: Int,
    completedValue: Int,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(Color.Transparent)
            .padding(horizontal = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = name,
            fontSize = 16.sp
        )
            Text(
                text = if (targetValue == 0) "--"
                else "${completedValue}/${targetValue}",
                style = TextStyle(
                    background = if (completedValue == targetValue && targetValue != 0) childCardsColor
                    else Color.Transparent,
                    fontSize = 16.sp
                ),
                modifier = Modifier
                    .padding(8.dp)
                    .width(50.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
    }
}