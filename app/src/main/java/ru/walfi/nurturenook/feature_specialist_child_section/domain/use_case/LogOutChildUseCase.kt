package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class LogOutChildUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    suspend operator fun invoke(){
        repository.logOutChild()
    }
}