package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class DeleteCommentUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    suspend operator fun invoke(comment: Comment){
        repository.deleteComment(comment)
    }
}