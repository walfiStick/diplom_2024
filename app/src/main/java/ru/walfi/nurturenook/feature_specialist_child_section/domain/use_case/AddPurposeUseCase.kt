package ru.walfi.nurturenook.feature_specialist_child_section.domain.use_case

import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_specialist_child_section.domain.repository.SpecialistChildSectionRepository

class AddPurposeUseCase(
    private val repository: SpecialistChildSectionRepository
) {

    suspend operator fun invoke(purpose: Purpose){
        repository.insertPurpose(purpose)
    }
}