package ru.walfi.nurturenook.feature_specialist_child_section.presentation.create_tasks

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreateTasksViewModel @Inject constructor(): ViewModel() {

    val state = CreateTasksState()
}