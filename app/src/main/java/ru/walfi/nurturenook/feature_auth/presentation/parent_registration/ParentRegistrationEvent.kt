package ru.walfi.nurturenook.feature_auth.presentation.parent_registration

import ru.walfi.nurturenook.feature_auth.presentation.util.ParentFieldType

sealed class ParentRegistrationEvent{

    data class EnteredNewText(val value: String, val field: ParentFieldType): ParentRegistrationEvent()

    object PressedRegistrationButton : ParentRegistrationEvent()
    object PressedBackButton : ParentRegistrationEvent()
}