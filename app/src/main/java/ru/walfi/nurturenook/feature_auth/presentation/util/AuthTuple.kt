package ru.walfi.nurturenook.feature_auth.presentation.util

import androidx.room.ColumnInfo

data class AuthTuple(
    @ColumnInfo(name = "password") val password: String?,
    @ColumnInfo(name = "id") val id: Int?,
)
