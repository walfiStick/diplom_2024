package ru.walfi.nurturenook.feature_auth.presentation.util

import ru.walfi.nurturenook.core.model.Specialist

data class RegistrationSpecialistData(
    val firstName: String,
    val lastName: String,
    val patronymic: String,
    val profession: String,
    val email: String,
    val phone: String,
    val organization: String,
    val password: String,
    val confirm: String,
){

    fun toSpecialist() = Specialist(
        firstName = firstName,
        lastName = lastName,
        patronymic = patronymic,
        profession = profession,
        email = email,
        phone = phone,
        organization = organization,
        password = password
    )
}