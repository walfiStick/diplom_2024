package ru.walfi.nurturenook.feature_auth.presentation.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthUseCases
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val authUseCases: AuthUseCases,
): ViewModel(){

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun checkAuthorization() {
        viewModelScope.launch {
            val account = authUseCases.checkAccountDataUseCase()
            when(account.role){
                "specialist" -> {
                    _eventFlow.emit(
                        UiEvent.OpenSpecialistHomeScreen(account.accountId)
                    )
                }
                "parent" -> {
                    _eventFlow.emit(
                        UiEvent.OpenParentHomeScreen
                    )
                }
                "child" -> {
                    _eventFlow.emit(
                        UiEvent.OpenChildHomeScreen
                    )
                }
                else -> {
                    _eventFlow.emit(
                        UiEvent.OpenAuthorizationScreen
                    )
                }
            }
        }
    }

    sealed class UiEvent{
        object OpenAuthorizationScreen: UiEvent()
        class OpenSpecialistHomeScreen(val id: Int): UiEvent()
        object OpenParentHomeScreen: UiEvent()
        object OpenChildHomeScreen: UiEvent()
    }
}