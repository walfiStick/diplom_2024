package ru.walfi.nurturenook.feature_auth.domain.use_case

import android.database.sqlite.SQLiteException
import ru.walfi.nurturenook.core.model.InvalidParentException
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository
import ru.walfi.nurturenook.feature_auth.presentation.util.RegistrationParentData

class AddParentUseCase(
    private val repository: AuthRepository
) {

    @Throws(InvalidParentException::class)
    suspend operator fun invoke(registrationParentData: RegistrationParentData){
        if(registrationParentData.lastName.isBlank()){
            throw InvalidParentException("Поле 'Фамилия' не может быть пустым")
        }
        if(registrationParentData.firstName.isBlank()){
            throw InvalidParentException("Поле 'Имя' не может быть пустым")
        }
        if(registrationParentData.patronymic.isBlank()){
            throw InvalidParentException("Поле 'Отчество' не может быть пустым")
        }
        if(registrationParentData.email.isBlank()){
            throw InvalidParentException("Поле 'Почта' не может быть пустым")
        }
        if(registrationParentData.password.isBlank()){
            throw InvalidParentException("Поле 'Пароль' не может быть пустым")
        }
        if(registrationParentData.password != registrationParentData.confirm){
            throw InvalidParentException("Пароли не совпадают")
        }
        try{
            repository.insertParent(registrationParentData.toParent())
        }catch (e: SQLiteException){
            throw SQLiteException("Ошибка регистрации. Возможно указанная почта уже зарегистрирована в приложении")
        }
    }

}