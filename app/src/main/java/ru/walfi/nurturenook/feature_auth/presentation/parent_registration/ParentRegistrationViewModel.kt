package ru.walfi.nurturenook.feature_auth.presentation.parent_registration

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthUseCases
import ru.walfi.nurturenook.feature_auth.presentation.util.RegistrationParentData
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.ParentFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.ParentFieldType
import javax.inject.Inject

@HiltViewModel
class ParentRegistrationViewModel @Inject constructor(
    private val authUseCases: AuthUseCases
) : ViewModel() {

    private val _state = mutableStateOf(
        ParentRegistrationState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: ParentRegistrationEvent) {
        when (event) {
            is ParentRegistrationEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
            is ParentRegistrationEvent.PressedRegistrationButton -> {
                viewModelScope.launch {
                    _state.value = state.value.copy(isEnabledButton = false)
                    try {
                        authUseCases.addParentUseCase(
                            RegistrationParentData(
                                firstName = state.value.firstName.textFieldState.text,
                                lastName = state.value.lastName.textFieldState.text,
                                patronymic = state.value.patronymic.textFieldState.text,
                                email = state.value.email.textFieldState.text,
                                password = state.value.password.textFieldState.text,
                                confirm = state.value.confirm.textFieldState.text
                            )
                        )
                        _eventFlow.emit(UiEvent.OpenAuthorizationScreen)
                    } catch (e: Exception) {
                        _state.value = state.value.copy(isEnabledButton = true)
                        _eventFlow.emit(
                            UiEvent.ShowSnackbar(
                                message = e.message ?: "Что-то пошло не так"
                            )
                        )
                    }
                }
            }
            is ParentRegistrationEvent.EnteredNewText ->{
                when(event.field){
                    ParentFieldType.LastName -> {
                        _state.value = state.value.copy(
                            lastName = ParentFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Фамилия"),
                                type = event.field
                            )
                        )
                    }
                    ParentFieldType.FirstName -> {
                        _state.value = state.value.copy(
                            firstName = ParentFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Имя"),
                                type = event.field
                            )
                        )
                    }
                    ParentFieldType.Patronymic -> {
                        _state.value = state.value.copy(
                            patronymic = ParentFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Отчество"),
                                type = event.field
                            )
                        )
                    }
                    ParentFieldType.Email -> {
                        _state.value = state.value.copy(
                            email = ParentFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Почта"),
                                type = event.field
                            )
                        )
                    }
                    ParentFieldType.Password -> {
                        _state.value = state.value.copy(
                            password = ParentFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Пароль"),
                                type = event.field,
                                isPassword = true
                            )
                        )
                    }
                    ParentFieldType.Confirm -> {
                        _state.value = state.value.copy(
                            confirm = ParentFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Подтвердите пароль"),
                                type = event.field,
                                isPassword = true
                            )
                        )
                    }
                }
            }
        }
    }

    sealed class UiEvent {
        data class ShowSnackbar(val message: String) : UiEvent()
        object OpenAuthorizationScreen : UiEvent()
        object NavigateUp : UiEvent()
    }
}