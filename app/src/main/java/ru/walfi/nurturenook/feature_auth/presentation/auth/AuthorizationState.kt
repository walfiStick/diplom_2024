package ru.walfi.nurturenook.feature_auth.presentation.auth

import ru.walfi.nurturenook.core.util.Role
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.ToggleableInfo

data class AuthorizationState(
    val selectedRole: Role? = null,
    val login: TextFieldState = TextFieldState(hint = "Логин"),
    val password: TextFieldState = TextFieldState(hint = "Пароль"),
    val code: TextFieldState = TextFieldState(hint = "Уникальный код"),
    val radioButtons: List<ToggleableInfo> = listOf(
        ToggleableInfo(
            isChecked = false,
            text = "Специалист",
            role = Role.Specialist
        ),
        ToggleableInfo(
            isChecked = false,
            text = "Родитель",
            role = Role.Parent
        ),
        ToggleableInfo(
            isChecked = false,
            text = "Ребёнок",
            role = Role.Child
        )
    ),
    val isAuthProcess: Boolean = false
)