package ru.walfi.nurturenook.feature_auth.presentation.specialist_registration

import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.SpecialistsFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.SpecialistsFieldType

data class SpecialistRegistrationState(
    val toolbarText: String = "Регистрация",
    val lastName: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Фамилия"),
        SpecialistsFieldType.LastName
    ),
    val firstName: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Имя"),
        SpecialistsFieldType.FirstName
    ),
    val patronymic: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Отчество"),
        SpecialistsFieldType.Patronymic
    ),
    val profession: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Профессия"),
        SpecialistsFieldType.Profession
    ),
    val email: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Почта"),
        SpecialistsFieldType.Email
    ),
    val phone: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Телефон"),
        SpecialistsFieldType.Phone
    ),
    val organization: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Организация"),
        SpecialistsFieldType.Organization
    ),
    val password: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Пароль"),
        SpecialistsFieldType.Password,
        isPassword = true
    ),
    val confirm: SpecialistsFieldState = SpecialistsFieldState(
        TextFieldState(hint = "Подтвердите пароль"),
        SpecialistsFieldType.Confirm,
        isPassword = true
    ),
    val isEnabledButton: Boolean = true
)