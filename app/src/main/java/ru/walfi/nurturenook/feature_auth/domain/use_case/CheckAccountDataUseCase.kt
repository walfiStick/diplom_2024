package ru.walfi.nurturenook.feature_auth.domain.use_case

import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository

class CheckAccountDataUseCase(
    private val repository: AuthRepository
) {

    suspend operator fun invoke(): SavedAccountData {
        return repository.getAccountData().first()
    }
}