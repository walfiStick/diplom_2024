package ru.walfi.nurturenook.feature_auth.presentation.specialist_registration

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthUseCases
import ru.walfi.nurturenook.feature_auth.presentation.util.RegistrationSpecialistData
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.SpecialistsFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.SpecialistsFieldType
import javax.inject.Inject

@HiltViewModel
class SpecialistRegistrationViewModel @Inject constructor(
    private val authUseCases: AuthUseCases
) : ViewModel() {

    private val _state = mutableStateOf(
        SpecialistRegistrationState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: SpecialistRegistrationEvent) {
        when (event) {
            is SpecialistRegistrationEvent.PressedBackButton -> {
                viewModelScope.launch {
                    _eventFlow.emit(UiEvent.NavigateUp)
                }
            }
            is SpecialistRegistrationEvent.PressedRegistrationButton -> {
                viewModelScope.launch {
                    _state.value = state.value.copy(isEnabledButton = false)
                    try {
                        authUseCases.addSpecialistUseCase(
                            RegistrationSpecialistData(
                                firstName = state.value.firstName.textFieldState.text,
                                lastName = state.value.lastName.textFieldState.text,
                                patronymic = state.value.patronymic.textFieldState.text,
                                profession = state.value.profession.textFieldState.text,
                                email = state.value.email.textFieldState.text,
                                phone = state.value.phone.textFieldState.text,
                                organization = state.value.organization.textFieldState.text,
                                password = state.value.password.textFieldState.text,
                                confirm = state.value.confirm.textFieldState.text,
                            )
                        )
                        _eventFlow.emit(UiEvent.OpenAuthorizationScreen)
                    } catch (e: Exception) {
                        _state.value = state.value.copy(isEnabledButton = true)
                        _eventFlow.emit(
                            UiEvent.ShowSnackbar(
                                message = e.message ?: "Что-то пошло не так"
                            )
                        )
                    }
                }
            }
            is SpecialistRegistrationEvent.EnteredNewText -> {
                when(event.field){
                    SpecialistsFieldType.LastName ->{
                        _state.value = state.value.copy(
                            lastName = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Фамилия"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.FirstName ->{
                        _state.value = state.value.copy(
                            firstName = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Имя"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.Patronymic ->{
                        _state.value = state.value.copy(
                            patronymic = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Отчество"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.Profession ->{
                        _state.value = state.value.copy(
                            profession = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Профессия"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.Email ->{
                        _state.value = state.value.copy(
                            email = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Почта"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.Phone ->{
                        _state.value = state.value.copy(
                            phone = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Телефон"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.Organization ->{
                        _state.value = state.value.copy(
                            organization = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Организация"),
                                type = event.field
                            )
                        )
                    }
                    SpecialistsFieldType.Password ->{
                        _state.value = state.value.copy(
                            password = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Пароль"),
                                type = event.field,
                                isPassword = true
                            )
                        )
                    }
                    SpecialistsFieldType.Confirm ->{
                        _state.value = state.value.copy(
                            confirm = SpecialistsFieldState(
                                textFieldState = TextFieldState(text = event.value, hint = "Подтвердите пароль"),
                                type = event.field,
                                isPassword = true
                            )
                        )
                    }
                }
            }
        }
    }


    sealed class UiEvent {
        data class ShowSnackbar(val message: String) : UiEvent()
        object OpenAuthorizationScreen : UiEvent()
        object NavigateUp : UiEvent()
    }
}