package ru.walfi.nurturenook.feature_auth.presentation.util

import ru.walfi.nurturenook.core.util.TextFieldState

class SpecialistsFieldState(
    val textFieldState: TextFieldState,
    val type: SpecialistsFieldType,
    val isPassword: Boolean = false
)

enum class SpecialistsFieldType{
    LastName,
    FirstName,
    Patronymic,
    Profession,
    Email,
    Phone,
    Organization,
    Password,
    Confirm
}