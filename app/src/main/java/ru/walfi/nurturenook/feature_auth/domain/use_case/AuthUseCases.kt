package ru.walfi.nurturenook.feature_auth.domain.use_case

data class AuthUseCases(
    val addParentUseCase: AddParentUseCase,
    val addSpecialistUseCase: AddSpecialistUseCase,
    val authSpecialistUseCase: AuthSpecialistUseCase,
    val checkAccountDataUseCase: CheckAccountDataUseCase,
    val saveAccountDataUseCase: SaveAccountDataUseCase,
    val authParentUseCase: AuthParentUseCase,
    val authChildUseCase: AuthChildUseCase
)
