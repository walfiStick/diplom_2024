package ru.walfi.nurturenook.feature_auth.presentation.auth

import ru.walfi.nurturenook.core.util.Role

sealed class AuthorizationEvent{

    data class EnteredLogin(val value: String): AuthorizationEvent()
    data class EnteredPassword(val value: String): AuthorizationEvent()
    data class EnteredCode(val value: String): AuthorizationEvent()

    data class ChangeRole(val role: Role): AuthorizationEvent()

    object PressedLogInButton: AuthorizationEvent()
    object PressedRegistrationButton : AuthorizationEvent()
}