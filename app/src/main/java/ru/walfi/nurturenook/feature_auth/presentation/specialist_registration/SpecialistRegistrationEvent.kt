package ru.walfi.nurturenook.feature_auth.presentation.specialist_registration

import ru.walfi.nurturenook.feature_auth.presentation.util.SpecialistsFieldType

sealed class SpecialistRegistrationEvent{

    data class EnteredNewText(val value: String, val field: SpecialistsFieldType): SpecialistRegistrationEvent()

    object PressedRegistrationButton : SpecialistRegistrationEvent()
    object PressedBackButton : SpecialistRegistrationEvent()
}