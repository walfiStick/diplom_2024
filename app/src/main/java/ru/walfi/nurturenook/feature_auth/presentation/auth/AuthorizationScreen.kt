package ru.walfi.nurturenook.feature_auth.presentation.auth

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Key
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.BaseButton
import ru.walfi.nurturenook.core.util.BaseTextFieldWithIcon
import ru.walfi.nurturenook.core.util.Role
import ru.walfi.nurturenook.ui.theme.backgroundColor
import ru.walfi.nurturenook.ui.theme.selectedRadioButtonColor
import ru.walfi.nurturenook.ui.theme.unselectedRadioButtonColor

@Composable
fun AuthorizationScreen(
    navController: NavController,
    viewModel: AuthorizationViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true){
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is AuthorizationViewModel.UiEvent.OpenRegistrationScreen -> {
                    navController.navigate(event.screen.route)
                }
                is AuthorizationViewModel.UiEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(message = event.message)
                }
                is AuthorizationViewModel.UiEvent.OpenHomeScreen -> {
                    navController.navigate(event.screen.route + "specialistId=${event.id}")
                }
                is AuthorizationViewModel.UiEvent.OpenParentHomeScreen -> {
                    navController.navigate(event.screen.route)
                }
                is AuthorizationViewModel.UiEvent.OpenChildHomeScreen -> {
                    navController.navigate(event.screen.route)
                }
            }
        }
    }

    Scaffold(
        modifier = Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .background(backgroundColor),
        scaffoldState = scaffoldState
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.weight(1f)
            ) {
                Column(
                    horizontalAlignment = Alignment.Start
                ) {
                    state.radioButtons.forEach { info ->
                        Row(
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = info.isChecked,
                                colors = RadioButtonDefaults.colors(
                                    selectedColor = selectedRadioButtonColor,
                                    unselectedColor = unselectedRadioButtonColor
                                ),
                                onClick = {viewModel.onEvent(AuthorizationEvent.ChangeRole(info.role))}
                            )
                            Text(text = info.text)
                        }
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
                AnimatedVisibility(
                    visible = state.selectedRole != Role.Child,
                    enter = fadeIn() + slideInVertically(),
                    exit = fadeOut() + slideOutVertically()
                ) {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        BaseTextFieldWithIcon(
                            text = state.login.text,
                            hint = state.login.hint,
                            icon = Icons.Filled.Person,
                            onValueChange = {viewModel.onEvent(AuthorizationEvent.EnteredLogin(it))}
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        BaseTextFieldWithIcon(
                            text = state.password.text,
                            hint = state.password.hint,
                            icon = Icons.Filled.Lock,
                            onValueChange = {viewModel.onEvent(AuthorizationEvent.EnteredPassword(it))},
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
                        )
                    }
                }
                AnimatedVisibility(
                    visible = state.selectedRole == Role.Child,
                    enter = fadeIn() + slideInVertically(),
                    exit = fadeOut() + slideOutVertically()
                ){
                    BaseTextFieldWithIcon(
                        text = state.code.text,
                        hint = state.code.hint,
                        icon = Icons.Filled.Key,
                        onValueChange = {viewModel.onEvent(AuthorizationEvent.EnteredCode(it))}
                    )
                }
            }
            Column(
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                BaseButton(text = "Войти", onClick = {viewModel.onEvent(AuthorizationEvent.PressedLogInButton)})
                Spacer(modifier = Modifier.height(8.dp))
                AnimatedVisibility(
                    visible = state.selectedRole != Role.Child,
                    enter = fadeIn() + slideInVertically(),
                    exit = fadeOut() + slideOutVertically()
                ) {
                    BaseButton(text = "Регистрация", onClick = {viewModel.onEvent(AuthorizationEvent.PressedRegistrationButton)})
                }
                Spacer(modifier = Modifier.height(24.dp))
            }
        }

    }
}