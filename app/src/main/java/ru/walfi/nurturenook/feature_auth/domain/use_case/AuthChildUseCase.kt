package ru.walfi.nurturenook.feature_auth.domain.use_case

import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository

class AuthChildUseCase(
    private val repository: AuthRepository
) {

    suspend operator fun invoke(childCode: String) : Int?{
        return repository.getChildIdByChildCode(childCode = childCode)
    }
}