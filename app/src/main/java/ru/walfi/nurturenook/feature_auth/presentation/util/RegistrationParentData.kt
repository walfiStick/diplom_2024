package ru.walfi.nurturenook.feature_auth.presentation.util

import ru.walfi.nurturenook.core.model.Parent

data class RegistrationParentData(
    val firstName: String,
    val lastName: String,
    val patronymic: String,
    val email: String,
    val password: String,
    val confirm: String,
){

    fun toParent() = Parent(
        firstName = firstName,
        lastName = lastName,
        patronymic = patronymic,
        email = email,
        password = password
    )
}
