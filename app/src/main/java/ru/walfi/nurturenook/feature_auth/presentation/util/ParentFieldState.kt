package ru.walfi.nurturenook.feature_auth.presentation.util

import ru.walfi.nurturenook.core.util.TextFieldState

class ParentFieldState(
    val textFieldState: TextFieldState,
    val type: ParentFieldType,
    val isPassword: Boolean = false
)

enum class ParentFieldType{
    LastName,
    FirstName,
    Patronymic,
    Email,
    Password,
    Confirm
}