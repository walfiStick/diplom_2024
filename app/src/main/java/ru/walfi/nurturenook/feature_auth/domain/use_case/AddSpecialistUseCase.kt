package ru.walfi.nurturenook.feature_auth.domain.use_case

import android.database.sqlite.SQLiteException
import ru.walfi.nurturenook.core.model.InvalidSpecialistException
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository
import ru.walfi.nurturenook.feature_auth.presentation.util.RegistrationSpecialistData

class AddSpecialistUseCase(
    private val repository: AuthRepository
) {

    @Throws(InvalidSpecialistException::class)
    suspend operator fun invoke(registrationSpecialistData: RegistrationSpecialistData){
        if(registrationSpecialistData.lastName.isBlank()){
            throw InvalidSpecialistException("Поле 'Фамилия' не может быть пустым")
        }
        if(registrationSpecialistData.firstName.isBlank()){
            throw InvalidSpecialistException("Поле 'Имя' не может быть пустым")
        }
        if(registrationSpecialistData.patronymic.isBlank()){
            throw InvalidSpecialistException("Поле 'Отчество' не может быть пустым")
        }
        if(registrationSpecialistData.profession.isBlank()){
            throw InvalidSpecialistException("Поле 'Профессия' не может быть пустым")
        }
        if(registrationSpecialistData.email.isBlank()){
            throw InvalidSpecialistException("Поле 'Почта' не может быть пустым")
        }
        if(registrationSpecialistData.phone.isBlank()){
            throw InvalidSpecialistException("Поле 'Телефон' не может быть пустым")
        }
        if(registrationSpecialistData.organization.isBlank()){
            throw InvalidSpecialistException("Поле 'Организация' не может быть пустым")
        }
        if(registrationSpecialistData.password.isBlank()){
            throw InvalidSpecialistException("Поле 'Пароль' не может быть пустым")
        }
        if(registrationSpecialistData.password != registrationSpecialistData.confirm){
            throw InvalidSpecialistException("Пароли не совпадают")
        }
        try{
            repository.insertSpecialist(registrationSpecialistData.toSpecialist())
        }catch (e: SQLiteException){
            throw SQLiteException("Ошибка регистрации. Возможно указанная почта уже зарегистрирована в приложении")
        }
    }
}