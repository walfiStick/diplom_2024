package ru.walfi.nurturenook.feature_auth.domain.use_case

import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository

class SaveAccountDataUseCase(
    private val repository: AuthRepository
) {

    suspend operator fun invoke(role: String, id: Int){
        repository.saveAccountData(
            SavedAccountData(
                role = role,
                accountId = id
            )
        )
    }
}