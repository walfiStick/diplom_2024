@file:OptIn(ExperimentalMaterial3Api::class)

package ru.walfi.nurturenook.feature_auth.presentation.parent_registration

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.BaseButton
import ru.walfi.nurturenook.core.util.BaseTextField
import ru.walfi.nurturenook.core.util.LoadingAnimation
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_auth.presentation.specialist_registration.SpecialistRegistrationEvent
import ru.walfi.nurturenook.ui.theme.backgroundColor

@Composable
fun ParentRegistrationScreen(
    navController: NavController,
    viewModel: ParentRegistrationViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val items = listOf(
        state.lastName,
        state.firstName,
        state.patronymic,
        state.email,
        state.password,
        state.confirm
    )

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is ParentRegistrationViewModel.UiEvent.NavigateUp -> {
                    navController.navigateUp()
                }
                is ParentRegistrationViewModel.UiEvent.OpenAuthorizationScreen -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = "Регистрация прошла успешно!"
                    )
                    navController.navigate(Screen.AuthorizationScreen.route)
                }
                is ParentRegistrationViewModel.UiEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
            }
        }
    }


    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .systemBarsPadding(),
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                navigationIcon = {
                    IconButton(onClick = { viewModel.onEvent(ParentRegistrationEvent.PressedBackButton) }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Назад"
                        )
                    }
                }
            )
        }
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            AnimatedVisibility(visible = state.isEnabledButton) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.fillMaxSize()
                ) {
                    Spacer(modifier = Modifier.height(16.dp))
                    LazyColumn(
                        modifier = Modifier.weight(1f),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        items(items) { fieldState ->
                            BaseTextField(
                                text = fieldState.textFieldState.text,
                                hint = fieldState.textFieldState.hint,
                                onValueChange = {
                                    viewModel.onEvent(
                                        ParentRegistrationEvent.EnteredNewText(
                                            it,
                                            fieldState.type
                                        )
                                    )
                                },
                                keyboardOptions = if (fieldState.isPassword)
                                    KeyboardOptions(keyboardType = KeyboardType.Password)
                                else KeyboardOptions()
                            )
                            Spacer(modifier = Modifier.height(8.dp))
                        }
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    BaseButton(
                        text = "Зарегистрироваться",
                        enabled = state.isEnabledButton,
                        onClick = {
                            viewModel.onEvent(ParentRegistrationEvent.PressedRegistrationButton)
                        }
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                }
            }
            AnimatedVisibility(visible = !state.isEnabledButton) {
                LoadingAnimation()
            }
        }
    }
}