package ru.walfi.nurturenook.feature_auth.presentation.parent_registration

import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.ParentFieldState
import ru.walfi.nurturenook.feature_auth.presentation.util.ParentFieldType

data class ParentRegistrationState(
    val toolbarText: String = "Регистрация",
    val lastName: ParentFieldState = ParentFieldState(
        TextFieldState(hint = "Фамилия"),
        ParentFieldType.LastName
    ),
    val firstName: ParentFieldState = ParentFieldState(
        TextFieldState(hint = "Имя"),
        ParentFieldType.FirstName
    ),
    val patronymic: ParentFieldState = ParentFieldState(
        TextFieldState(hint = "Отчество"),
        ParentFieldType.Patronymic
    ),
    val email: ParentFieldState = ParentFieldState(
        TextFieldState(hint = "Почта"),
        ParentFieldType.Email
    ),
    val password: ParentFieldState = ParentFieldState(
        TextFieldState(hint = "Пароль"),
        ParentFieldType.Password,
        isPassword = true
    ),
    val confirm: ParentFieldState = ParentFieldState(
        TextFieldState(hint = "Подтвердите пароль"),
        ParentFieldType.Confirm,
        isPassword = true
    ),
    val isEnabledButton: Boolean = true
)