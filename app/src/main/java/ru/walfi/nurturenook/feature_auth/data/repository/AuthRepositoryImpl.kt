package ru.walfi.nurturenook.feature_auth.data.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.feature_auth.data.data_source.AuthDao
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository
import ru.walfi.nurturenook.feature_auth.presentation.util.AuthTuple

class AuthRepositoryImpl(
    private val dao: AuthDao,
    private val dataStoreManager: DataStoreManager
): AuthRepository {

    override suspend fun insertParent(parent: Parent) {
        dao.insertParent(parent)
    }

    override suspend fun insertSpecialist(specialist: Specialist) {
        dao.insertSpecialist(specialist)
    }

    override suspend fun getAuthSpecialistTupleByEmail(email: String): AuthTuple? {
        return dao.getAuthSpecialistTupleByEmail(email)
    }

    override suspend fun getAuthParentTupleByEmail(email: String): AuthTuple? {
        return dao.getAuthParentTupleByEmail(email)
    }

    override suspend fun getChildIdByChildCode(childCode: String): Int? {
        return dao.getChildIdByChildCode(childCode)
    }

    override suspend fun saveAccountData(savedAccountData: SavedAccountData) {
        dataStoreManager.saveAccountData(savedAccountData)
    }

    override fun getAccountData(): Flow<SavedAccountData> {
        return dataStoreManager.getAccountData()
    }
}