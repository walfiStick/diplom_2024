package ru.walfi.nurturenook.feature_auth.presentation.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.R
import ru.walfi.nurturenook.core.util.Screen

@Composable
fun SplashScreen(
    navController: NavController,
    viewModel: SplashViewModel = hiltViewModel()
){
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(key1 = lifecycleOwner){
        val observer = LifecycleEventObserver{_, event ->
            when(event){
                Lifecycle.Event.ON_CREATE -> {
                    viewModel.checkAuthorization()
                }
                else -> {}
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    LaunchedEffect(key1 = true){
        viewModel.eventFlow.collectLatest { event ->
            when(event){
                is SplashViewModel.UiEvent.OpenSpecialistHomeScreen -> {
                    navController.navigate(Screen.SpecialistHomeScreen.route + "specialistId=${event.id}")
                }
                is SplashViewModel.UiEvent.OpenAuthorizationScreen -> {
                    navController.navigate(Screen.AuthorizationScreen.route)
                }
                is SplashViewModel.UiEvent.OpenParentHomeScreen -> {
                    navController.navigate(Screen.ParentHomeScreen.route)
                }
                is SplashViewModel.UiEvent.OpenChildHomeScreen -> {
                    navController.navigate(Screen.ChildHomeScreen.route)
                }
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .systemBarsPadding(),
        contentAlignment = Alignment.Center
    ){
        Image(
            painter = painterResource(id = R.drawable.ic_splash),
            contentDescription = "Splash"
        )
    }
}