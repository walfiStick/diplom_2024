package ru.walfi.nurturenook.feature_auth.domain.use_case

import android.database.sqlite.SQLiteException
import ru.walfi.nurturenook.feature_auth.domain.repository.AuthRepository

class AuthSpecialistUseCase(
    private val repository: AuthRepository
) {

    @Throws(SQLiteException::class)
    suspend operator fun invoke(email: String, password: String): Int?{
        try{
            val data = repository.getAuthSpecialistTupleByEmail(email)
            return if(data?.password == password) data.id else null
        }catch (e:SQLiteException){
            throw SQLiteException("Ошибка авторизации")
        }
    }
}