package ru.walfi.nurturenook.feature_auth.presentation.auth

import android.database.sqlite.SQLiteException
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.core.util.Role
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.core.util.TextFieldState
import ru.walfi.nurturenook.feature_auth.domain.use_case.AuthUseCases
import javax.inject.Inject

@HiltViewModel
class AuthorizationViewModel @Inject constructor(
    private val authUseCases: AuthUseCases,
): ViewModel(){

    private val _state = mutableStateOf(
        AuthorizationState()
    )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: AuthorizationEvent){
        when(event){
            is AuthorizationEvent.ChangeRole ->{
                _state.value = state.value.copy(
                    selectedRole = event.role,
                    radioButtons = state.value.radioButtons.map {
                        it.copy(
                            isChecked = event.role == it.role
                        )
                    }
                )
            }
            is AuthorizationEvent.EnteredLogin -> {
                _state.value = state.value.copy(
                    login = TextFieldState(text = event.value, hint = "Логин")
                )
            }
            is AuthorizationEvent.EnteredPassword -> {
                _state.value = state.value.copy(
                    password = TextFieldState(text = event.value, hint = "Пароль")
                )
            }
            is AuthorizationEvent.EnteredCode -> {
                _state.value = state.value.copy(
                    code = TextFieldState(text = event.value, hint = "Уникальный код")
                )
            }
            is AuthorizationEvent.PressedRegistrationButton -> {
                when(_state.value.selectedRole){
                    null -> {
                        viewModelScope.launch {
                            _eventFlow.emit(UiEvent.ShowSnackbar(message = "Выберите роль"))
                        }
                    }
                    Role.Parent -> {
                        viewModelScope.launch {
                            _eventFlow.emit(UiEvent.OpenRegistrationScreen(Screen.ParentRegistrationScreen))
                        }
                    }
                    Role.Specialist -> {
                        viewModelScope.launch {
                            _eventFlow.emit(UiEvent.OpenRegistrationScreen(Screen.SpecialistRegistrationScreen))
                        }
                    }
                    Role.Child -> {/*for child registration not available*/}
                }
            }
            is AuthorizationEvent.PressedLogInButton -> {
                _state.value = state.value.copy(isAuthProcess = true)
                when(_state.value.selectedRole){
                    null -> {
                        viewModelScope.launch {
                            _eventFlow.emit(UiEvent.ShowSnackbar(message = "Выберите роль"))
                        }
                    }
                    Role.Specialist -> {
                        viewModelScope.launch {
                            try{
                                val id = authUseCases.authSpecialistUseCase(
                                    state.value.login.text,
                                    state.value.password.text
                                )
                                if (id != null){
                                    authUseCases.saveAccountDataUseCase(
                                        role = "specialist",
                                        id = id
                                    )
                                    _eventFlow.emit(
                                        UiEvent.OpenHomeScreen(Screen.SpecialistHomeScreen, id)
                                    )
                                }else{
                                    _eventFlow.emit(
                                        UiEvent.ShowSnackbar(
                                        message = "Почта или пароль введены неверно"
                                        )
                                    )
                                }
                            }catch (e: Exception){
                                _state.value = state.value.copy(isAuthProcess = false)
                                _eventFlow.emit(
                                    UiEvent.ShowSnackbar(
                                        message = e.message ?: "Что-то пошло не так"
                                    )
                                )
                            }
                        }
                    }
                    Role.Parent -> {
                        viewModelScope.launch {
                            try {
                                val id = authUseCases.authParentUseCase(
                                    _state.value.login.text,
                                    _state.value.password.text
                                )
                                if(id != null){
                                    authUseCases.saveAccountDataUseCase(
                                        role = "parent",
                                        id = id
                                    )
                                    _eventFlow.emit(
                                        UiEvent.OpenParentHomeScreen(Screen.ParentHomeScreen)
                                    )
                                }else{
                                    _eventFlow.emit(
                                        UiEvent.ShowSnackbar(
                                            message = "Почта или пароль введены неверно"
                                        )
                                    )
                                }
                            }catch (e: SQLiteException){
                                _state.value = state.value.copy(isAuthProcess = false)
                                _eventFlow.emit(
                                    UiEvent.ShowSnackbar(
                                        message = e.message ?: "Что-то пошло не так"
                                    )
                                )
                            }
                        }
                    }
                    Role.Child -> {
                        viewModelScope.launch {
                            try {
                                val id = authUseCases.authChildUseCase(_state.value.code.text)
                                if (id != null){
                                    authUseCases.saveAccountDataUseCase(
                                        role = "child",
                                        id = id
                                    )
                                    _eventFlow.emit(
                                        UiEvent.OpenChildHomeScreen(Screen.ChildHomeScreen)
                                    )
                                }else{
                                    _eventFlow.emit(
                                        UiEvent.ShowSnackbar(
                                            message = "Уникальный код неверный"
                                        )
                                    )
                                }
                            }catch (e: SQLiteException){
                                _state.value = state.value.copy(isAuthProcess = false)
                                _eventFlow.emit(
                                    UiEvent.ShowSnackbar(
                                        message = e.message ?: "Что-то пошло не так"
                                    )
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    sealed class UiEvent{
        data class ShowSnackbar(val message: String): UiEvent()
        class OpenRegistrationScreen(val screen: Screen) : UiEvent()
        class OpenHomeScreen(val screen: Screen, val id: Int) : UiEvent()
        class OpenParentHomeScreen(val screen: Screen) : UiEvent()
        class OpenChildHomeScreen(val screen: Screen) : UiEvent()
    }
}