package ru.walfi.nurturenook.feature_auth.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.feature_auth.presentation.util.AuthTuple

interface AuthRepository {

    suspend fun insertParent(parent: Parent)

    suspend fun insertSpecialist(specialist: Specialist)

    suspend fun getAuthSpecialistTupleByEmail(email: String): AuthTuple?

    suspend fun getAuthParentTupleByEmail(email: String): AuthTuple?

    suspend fun getChildIdByChildCode(childCode: String): Int?

    suspend fun saveAccountData(savedAccountData: SavedAccountData)

    fun getAccountData(): Flow<SavedAccountData>
}