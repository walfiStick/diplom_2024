package ru.walfi.nurturenook.feature_auth.data.data_source

import android.database.sqlite.SQLiteException
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.feature_auth.presentation.util.AuthTuple

@Dao
interface AuthDao {

    @Insert
    @Throws(SQLiteException::class)
    suspend fun insertParent(parent: Parent)

    @Insert
    @Throws(SQLiteException::class)
    suspend fun insertSpecialist(specialist: Specialist)

    @Query("SELECT password, id FROM specialist WHERE email = :email")
    suspend fun getAuthSpecialistTupleByEmail(email: String): AuthTuple?

    @Query("SELECT password, id FROM parent WHERE email = :email")
    suspend fun getAuthParentTupleByEmail(email: String): AuthTuple?

    @Query("SELECT id FROM child WHERE childCode = :childCode")
    suspend fun getChildIdByChildCode(childCode: String): Int?
}