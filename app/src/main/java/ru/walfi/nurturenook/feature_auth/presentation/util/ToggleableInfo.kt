package ru.walfi.nurturenook.feature_auth.presentation.util

import ru.walfi.nurturenook.core.util.Role

data class ToggleableInfo(
    val isChecked: Boolean,
    val text: String,
    val role: Role
)
