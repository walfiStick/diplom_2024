package ru.walfi.nurturenook.core.util

sealed class Screen(val route: String){

    //authorization
    object SplashScreen: Screen("splash_screen")
    object AuthorizationScreen: Screen("authorization_screen")
    object ParentRegistrationScreen: Screen("parent_registration_screen")
    object SpecialistRegistrationScreen: Screen("specialist_registration_screen")

    //specialist home
    object SpecialistHomeScreen: Screen("specialist_home_screen")
    object SpecialistChildListScreen: Screen("specialist_child_list_screen")
    object SpecialistProfileScreen: Screen("specialist_profile_screen")
    object AddChildScreen: Screen("add_child_screen")
    object SpecialistConnectScreen: Screen("specialist_connect_screen")

    //specialist child section
    object SpecialistChildSectionScreen: Screen("specialist_child_section_screen")
    object SpecialistChildProfileScreen: Screen("specialist_child_profile_screen")
    object SpecialistCreateTasksScreen: Screen("specialist_create_tasks_screen")
    object SpecialistSetPurposeScreen: Screen("specialist_set_purpose_screen")
    object SpecialistCommentsScreen: Screen("specialist_comments_screen")

    //parent home
    object ParentHomeScreen: Screen("parent_home_screen")
    object ParentChildListScreen: Screen("parent_child_list_screen")
    object ParentProfileScreen: Screen("parent_profile_screen")
    object ParentConnectScreen: Screen("parent_connect_screen")

    //parent child section
    object ParentChildSectionScreen: Screen("parent_child_section_screen")
    object ParentCommentsScreen: Screen("parent_comments_screen")
    object ParentInfoScreen: Screen("parent_info_screen")
    object ParentScheduleScreen: Screen("parent_schedule_screen")

    //game learning words
    object CreateWordLvlScreen: Screen("create_word_lvl_screen")
    object PlayWordLvlScreen: Screen("play_word_lvl_screen")

    //child home
    object ChildHomeScreen: Screen("child_home_screen")
    object ChildScheduleScreen: Screen("child_schedule_screen")
    object GameListScreen: Screen("game_list_screen")

    //animals
    object AnimalsScreen: Screen("animals_screen")
}