package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GameWordsLvl(
    val firstWord: String,
    val secondWord: String,
    val thirdWord: String,
    val answer: Int,
    val picture: String?,
    @PrimaryKey val id: Int? = null
)

class InvalidGameWordLvlException(message: String): Exception(message)
