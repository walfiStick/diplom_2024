package ru.walfi.nurturenook.core.model

import androidx.room.Entity

@Entity(primaryKeys = ["childCode", "specialistId"])
data class SpecialistToChild(
    val childCode: String,
    val specialistId: Int
)
