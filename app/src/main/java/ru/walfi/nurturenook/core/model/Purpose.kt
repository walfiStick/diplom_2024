package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["childId", "gameName"], unique = true)]
)
data class Purpose(
    val gameName: String,
    val childId: Int,
    val targetValue: Int,
    val completedValue: Int,
    @PrimaryKey val id: Int? = null
)

class InvalidPurposeException(message: String): Exception(message)
