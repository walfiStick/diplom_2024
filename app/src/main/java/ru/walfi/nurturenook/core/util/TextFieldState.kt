package ru.walfi.nurturenook.core.util

data class TextFieldState(
    val text: String = "",
    val hint: String = "",
)
