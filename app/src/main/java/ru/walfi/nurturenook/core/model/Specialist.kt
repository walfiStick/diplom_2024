package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["email"], unique = true)]
)
data class Specialist(
    val firstName: String,
    val lastName: String,
    val patronymic: String,
    val profession: String,
    val email: String,
    val phone: String,
    val organization: String,
    val password: String,
    @PrimaryKey val id: Int? = null
)

class InvalidSpecialistException(message: String) : Exception(message)
