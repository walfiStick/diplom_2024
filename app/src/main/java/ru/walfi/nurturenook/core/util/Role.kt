package ru.walfi.nurturenook.core.util

enum class Role {
    Specialist,
    Parent,
    Child
}