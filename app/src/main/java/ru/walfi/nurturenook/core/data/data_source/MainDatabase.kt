package ru.walfi.nurturenook.core.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.walfi.nurturenook.core.model.Child
import ru.walfi.nurturenook.core.model.Comment
import ru.walfi.nurturenook.core.model.GameWordsLvl
import ru.walfi.nurturenook.feature_auth.data.data_source.AuthDao
import ru.walfi.nurturenook.core.model.Parent
import ru.walfi.nurturenook.core.model.ParentToChild
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.core.model.Specialist
import ru.walfi.nurturenook.core.model.SpecialistToChild
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_child_home.data.data_source.ChildHomeDao
import ru.walfi.nurturenook.feature_game_words.data.data_source.GameWordsDao
import ru.walfi.nurturenook.feature_parent_child_section.data.data_source.ParentChildSectionDao
import ru.walfi.nurturenook.feature_parent_home.data.data_source.ParentHomeDao
import ru.walfi.nurturenook.feature_specialist_child_section.data.data_source.SpecialistChildSectionDao
import ru.walfi.nurturenook.feature_specialist_home.data.data_source.SpecialistHomeDao

@Database(
    entities = [
        Parent::class,
        Specialist::class,
        Child::class,
        SpecialistToChild::class,
        Comment::class,
        Purpose::class,
        ParentToChild::class,
        GameWordsLvl::class,
        Task::class
    ],
    version = 1
)
abstract class MainDatabase : RoomDatabase() {

    abstract fun authDao(): AuthDao
    abstract fun specialistHomeDao(): SpecialistHomeDao
    abstract fun parentHomeDao(): ParentHomeDao
    abstract fun specialistChildSectionDao(): SpecialistChildSectionDao
    abstract fun parentChildSectionDao(): ParentChildSectionDao
    abstract fun gameWordDao(): GameWordsDao
    abstract fun childHomeDao(): ChildHomeDao

    companion object {
        const val DATABASE_NAME = "auth_db"
    }
}