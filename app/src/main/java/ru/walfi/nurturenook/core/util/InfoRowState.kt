package ru.walfi.nurturenook.core.util

data class InfoRowState(
    val name: String,
    val text: String
)
