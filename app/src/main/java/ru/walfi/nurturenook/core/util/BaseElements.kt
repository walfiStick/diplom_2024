package ru.walfi.nurturenook.core.util

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Badge
import androidx.compose.material.BadgedBox
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Dangerous
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ru.walfi.nurturenook.ui.theme.backgroundColor
import ru.walfi.nurturenook.ui.theme.borderTextFieldColor
import ru.walfi.nurturenook.ui.theme.buttonColor
import ru.walfi.nurturenook.ui.theme.contentTextFieldColor
import ru.walfi.nurturenook.ui.theme.hintTextFieldColor
import ru.walfi.nurturenook.ui.theme.whiteColor
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.draw.clip
import ru.walfi.nurturenook.feature_parent_child_section.data.util.CommentData
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.util.PurposeInfo
import ru.walfi.nurturenook.ui.theme.backBasicTextColor
import ru.walfi.nurturenook.ui.theme.childCardsColor
import ru.walfi.nurturenook.ui.theme.commentCardsColor
import ru.walfi.nurturenook.ui.theme.loadingAnimationColor
import ru.walfi.nurturenook.ui.theme.secondButtonColor
import ru.walfi.nurturenook.ui.theme.secondTextColor
import ru.walfi.nurturenook.ui.theme.textColor

@Composable
fun BaseButton(
    text: String,
    onClick: () -> Unit,
    enabled: Boolean = true,
    modifier: Modifier = Modifier
) {
    Button(
        colors = ButtonDefaults.buttonColors(
            backgroundColor = buttonColor,
            contentColor = whiteColor
        ),
        enabled = enabled,
        onClick = onClick,
        shape = CircleShape,
        modifier = modifier
            .size(width = 200.dp, height = 50.dp),
    ) {
        Text(text = text)
    }
}

@Composable
fun BaseLoadButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Button(
        colors = ButtonDefaults.buttonColors(
            backgroundColor = secondButtonColor,
            contentColor = textColor
        ),
        onClick = onClick,
        shape = RoundedCornerShape(10.dp),
        modifier = modifier
//            .size(width = 200.dp, height = 50.dp),
    ) {
        Text(text = text)
    }
}

@Composable
fun BaseTextField(
    text: String,
    hint: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    keyboardOptions: KeyboardOptions = KeyboardOptions(),
) {
    TextField(
        value = text,
        onValueChange = onValueChange,
        shape = CircleShape,
        singleLine = true,
        modifier = modifier
            .widthIn(max = 250.dp)
            .border(shape = CircleShape, width = 2.dp, color = borderTextFieldColor),
        keyboardOptions = keyboardOptions,
        placeholder = { Text(text = hint, color = hintTextFieldColor) },
        textStyle = TextStyle(
            color = contentTextFieldColor,
            fontSize = 16.sp
        ),
        colors = TextFieldDefaults.textFieldColors(
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            cursorColor = hintTextFieldColor,
            backgroundColor = backgroundColor.copy(0f)
        )
    )
}

@Composable
fun BaseTextFieldWithIcon(
    text: String,
    hint: String,
    icon: ImageVector,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    keyboardOptions: KeyboardOptions = KeyboardOptions()
) {
    TextField(
        value = text,
        onValueChange = onValueChange,
        shape = CircleShape,
        singleLine = true,
        leadingIcon = { Icon(imageVector = icon, contentDescription = null) },
        modifier = modifier
            .widthIn(max = 250.dp)
            .border(shape = CircleShape, width = 2.dp, color = borderTextFieldColor),
        keyboardOptions = keyboardOptions,
        placeholder = { Text(text = hint, color = hintTextFieldColor) },
        textStyle = TextStyle(
            color = contentTextFieldColor,
            fontSize = 16.sp
        ),
        colors = TextFieldDefaults.textFieldColors(
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            cursorColor = hintTextFieldColor,
            backgroundColor = backgroundColor.copy(0f)
        )
    )
}

@Composable
fun LoadingAnimation(
    circleColor: Color = loadingAnimationColor,
    animationDelay: Int = 1000
) {
    // circle's scale state
    var circleScale by remember {
        mutableStateOf(0f)
    }

    // animation
    val circleScaleAnimate = animateFloatAsState(
        targetValue = circleScale,
        animationSpec = infiniteRepeatable(
            animation = tween(
                durationMillis = animationDelay
            )
        )
    )

    // This is called when the app is launched
    LaunchedEffect(Unit) {
        circleScale = 1f
    }

    // animating circle
    Box(
        modifier = Modifier
            .size(size = 64.dp)
            .scale(scale = circleScaleAnimate.value)
            .border(
                width = 4.dp,
                color = circleColor.copy(alpha = 1 - circleScaleAnimate.value),
                shape = CircleShape
            )
    )
}

@Composable
fun BaseInfoRow(
    infoRowState: InfoRowState,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .background(Color.Transparent)
            .fillMaxWidth()
            .padding(horizontal = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = infoRowState.name,
            style = TextStyle(
                color = secondTextColor
            ),
            fontSize = 16.sp
        )
        Text(
            text = infoRowState.text,
            style = TextStyle(
                color = textColor,
            ),
            fontSize = 16.sp
        )
    }
}

@Composable
fun BaseChildCard(
    firstName: String,
    lastName: String,
    parentPhone: String,
    onClick: () -> Unit = {},
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .clip(shape = RoundedCornerShape(12.dp))
            .clickable(onClick = onClick)
            .fillMaxWidth()
            .background(childCardsColor)
            .padding(8.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column {
            Text(
                text = firstName,
                style = TextStyle(
                    color = textColor,
                ),
                fontSize = 16.sp
            )
            Spacer(modifier = Modifier.height(2.dp))
            Text(
                text = lastName,
                style = TextStyle(
                    color = textColor,
                ),
                fontSize = 14.sp
            )
        }
        Text(
            text = parentPhone,
            style = TextStyle(
                color = textColor,
            ),
            fontSize = 18.sp
        )
    }
}

@Composable
fun BaseCommentCardForParent(
    commentData: CommentData,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .clip(shape = RoundedCornerShape(8.dp))
            .background(commentCardsColor)
            .clip(shape = RoundedCornerShape(8.dp))
            .padding(4.dp)
    ) {
        Text(text = commentData.content)
        Spacer(modifier = Modifier.height(2.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            Text(text = commentData.specialistLastName)
        }
    }
}

@Composable
fun BaseCommentCardForSpecialist(
    content: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .clip(shape = RoundedCornerShape(8.dp))
            .background(commentCardsColor)
            .clip(shape = RoundedCornerShape(8.dp))
            .padding(4.dp)
    ) {
        Text(text = content, modifier = Modifier.weight(1f))
        IconButton(onClick = onClick) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = "Удалить комментарий"
            )
        }
    }
}

@Composable
fun BaseClickableTextRow(
    text: String,
    onClick: () -> Unit,
    purpose: Int = 0
){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(8.dp))
            .background(backBasicTextColor)
            .padding(vertical = 8.dp, horizontal = 6.dp)
            .clickable(onClick = onClick),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = text,
            style = TextStyle(
                fontSize = 16.sp
            )
        )
        if(purpose != 0){
            Badge { Text(text = purpose.toString())}
        }
    }
}