package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Task(
    val description: String,
    val childId: Int,
    val executeFlag: Boolean,
    @PrimaryKey val id: Int? = null
)

class InvalidTaskException(message: String): Exception(message)
