package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["childCode"], unique = true)]
)
data class Child(
    val firstName: String,
    val lastName: String,
    val patronymic: String,
    val birthday: String,
    val disease: String,
    val parentFirstName: String,
    val parentLastName: String,
    val parentPatronymic: String,
    val parentPhone: String,
    val childCode: String,
    @PrimaryKey val id: Int? = null
)

class InvalidChildException(message: String): Exception(message)