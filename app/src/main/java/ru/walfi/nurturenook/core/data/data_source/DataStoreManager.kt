package ru.walfi.nurturenook.core.data.data_source

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.map
import ru.walfi.nurturenook.core.util.SavedAccountData

const val DATA_STORE_NAME = "data_store"
const val ACCOUNT_ROLE_KEY = "account_role_key"
const val ACCOUNT_ID_KEY = "account_id_key"
const val CHILD_ID_KEY = "child_id_key"


private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = DATA_STORE_NAME)

class DataStoreManager(private val context: Context) {

    suspend fun saveAccountData(savedAccountData: SavedAccountData){
        context.dataStore.edit {preferences ->
            preferences[stringPreferencesKey(ACCOUNT_ROLE_KEY)] = savedAccountData.role
            preferences[intPreferencesKey(ACCOUNT_ID_KEY)] = savedAccountData.accountId
        }
    }

    fun getAccountData() = context.dataStore.data.map {preferences ->
        return@map SavedAccountData(
            role = preferences[stringPreferencesKey(ACCOUNT_ROLE_KEY)] ?: "user isn't logged in",
            accountId = preferences[intPreferencesKey(ACCOUNT_ID_KEY)] ?: -1
        )
    }

    suspend fun saveChildId(id: Int){
        context.dataStore.edit { preferences ->
            preferences[intPreferencesKey(CHILD_ID_KEY)] = id
        }
    }

    fun getChildId() = context.dataStore.data.map { preferences ->
        preferences[intPreferencesKey(CHILD_ID_KEY)] ?: -1
    }
}