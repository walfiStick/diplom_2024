package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["email"], unique = true)]
)
data class Parent(
    val firstName: String,
    val lastName: String,
    val patronymic: String,
    val email: String,
    val password: String,
    @PrimaryKey val id: Int? = null
)

class InvalidParentException(message: String): Exception(message)
