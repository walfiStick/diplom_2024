package ru.walfi.nurturenook.core.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Comment(
    val content: String,
    val childId: Int,
    val specialistId: Int,
    @PrimaryKey val id: Int? = null
)

class InvalidCommentException(message: String): Exception(message)
