package ru.walfi.nurturenook.core.util

data class SavedAccountData(
    val role: String,
    val accountId: Int
)
