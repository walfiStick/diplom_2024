package ru.walfi.nurturenook.core

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_animals.AnimalsScreen
import ru.walfi.nurturenook.feature_auth.presentation.auth.AuthorizationScreen
import ru.walfi.nurturenook.feature_auth.presentation.parent_registration.ParentRegistrationScreen
import ru.walfi.nurturenook.feature_auth.presentation.specialist_registration.SpecialistRegistrationScreen
import ru.walfi.nurturenook.feature_auth.presentation.splash.SplashScreen
import ru.walfi.nurturenook.feature_child_home.presentation.home.ChildHomeScreen
import ru.walfi.nurturenook.feature_game_words.presentation.create_lvl.CreateWordLvlScreen
import ru.walfi.nurturenook.feature_game_words.presentation.play_lvl.PlayWordLvlScreen
import ru.walfi.nurturenook.feature_parent_child_section.presentation.child_section.ParentChildSectionScreen
import ru.walfi.nurturenook.feature_parent_home.presentation.connect.ParentConnectScreen
import ru.walfi.nurturenook.feature_parent_home.presentation.home.ParentHomeScreen
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.child_section.ChildSectionScreen
import ru.walfi.nurturenook.feature_specialist_home.presentation.add_child.AddChildScreen
import ru.walfi.nurturenook.feature_specialist_home.presentation.connect.SpecialistConnectScreen
import ru.walfi.nurturenook.feature_specialist_home.presentation.home.SpecialistHomeScreen

@Composable
fun Navigation(){
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.SplashScreen.route){

        composable(route = Screen.SplashScreen.route){
            SplashScreen(navController = navController)
        }
        composable(route = Screen.AuthorizationScreen.route){
            AuthorizationScreen(navController = navController)
        }
        composable(route = Screen.ParentRegistrationScreen.route){
            ParentRegistrationScreen(navController = navController)
        }
        composable(route = Screen.SpecialistRegistrationScreen.route){
            SpecialistRegistrationScreen(navController = navController)
        }
        composable(
            route = Screen.SpecialistHomeScreen.route + "specialistId={specialistId}",
            arguments = listOf(
                navArgument("specialistId"){ type = NavType.IntType }
            )
        ){
            SpecialistHomeScreen(navController = navController, it.arguments?.getInt("specialistId"))
        }
        composable(
            route = Screen.AddChildScreen.route + "specialistId={specialistId}",
            arguments = listOf(
                navArgument("specialistId"){ type = NavType.IntType }
            )
        ){
            AddChildScreen(navController = navController)
        }
        composable(
            route = Screen.SpecialistChildSectionScreen.route + "childId={childId}",
            arguments = listOf(
                navArgument("childId"){ type = NavType.IntType }
            )
        ){
            ChildSectionScreen(navController = navController, childId = it.arguments?.getInt("childId"))
        }
        composable(route = Screen.ParentHomeScreen.route){
            ParentHomeScreen(navController = navController)
        }
        composable(route = Screen.ParentConnectScreen.route){
            ParentConnectScreen(navController = navController)
        }
        composable(route = Screen.ParentChildSectionScreen.route){
            ParentChildSectionScreen(navController = navController)
        }
        composable(route = Screen.CreateWordLvlScreen.route){
            CreateWordLvlScreen(navController = navController)
        }
        composable(route = Screen.SpecialistConnectScreen.route){
            SpecialistConnectScreen(navController = navController)
        }
        composable(route = Screen.ChildHomeScreen.route){
            ChildHomeScreen(navController = navController)
        }
        composable(route = Screen.PlayWordLvlScreen.route){
            PlayWordLvlScreen(navController = navController)
        }
        composable(route = Screen.AnimalsScreen.route){
            AnimalsScreen(navController = navController)
        }
    }
}