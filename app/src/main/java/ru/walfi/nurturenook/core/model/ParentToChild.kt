package ru.walfi.nurturenook.core.model

import androidx.room.Entity

@Entity(
    primaryKeys = ["childCode", "parentId"]
)
data class ParentToChild(
    val childCode: String,
    val parentId: Int
)
