package ru.walfi.nurturenook

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NurtureNookApp: Application()