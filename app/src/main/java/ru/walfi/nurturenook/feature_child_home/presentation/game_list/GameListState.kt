package ru.walfi.nurturenook.feature_child_home.presentation.game_list

import ru.walfi.nurturenook.core.util.Screen

data class GameListState(
    val toolbarText: String = "Выбор игры",
    val gameList: List<ChooseGame> = listOf(
        ChooseGame(
            name = "Животные",
            screen = Screen.AnimalsScreen
        ),
        ChooseGame(
            name = "Изучение слов",
            screen = Screen.PlayWordLvlScreen
        )
    ),
    val learnWordPurpose: Int = 0
)

data class ChooseGame(
    val name: String,
    val screen: Screen
)