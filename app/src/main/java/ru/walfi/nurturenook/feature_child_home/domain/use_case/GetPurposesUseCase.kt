package ru.walfi.nurturenook.feature_child_home.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository

class GetPurposesUseCase(
    private val repository: ChildHomeRepository
) {

    operator fun invoke(childId: Int): Flow<List<Purpose>>{
        return repository.getPurposesByChildId(childId)
    }
}