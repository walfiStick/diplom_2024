package ru.walfi.nurturenook.feature_child_home.presentation.game_list

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import ru.walfi.nurturenook.core.util.BaseClickableTextRow
import ru.walfi.nurturenook.feature_specialist_child_section.presentation.create_tasks.TestTextRow

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GameListScreen(
    navController: NavController,
    viewModel: GameListViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { androidx.compose.material.Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                )
            )
        }
    ) {
        LazyColumn(modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp)
        ){
            items(state.gameList){
                Spacer(modifier = Modifier.height(4.dp))
                BaseClickableTextRow(
                    text = it.name,
                    onClick = {navController.navigate(it.screen.route)},
                    purpose = if(it.name == "Изучение слов") state.learnWordPurpose else 0
                )
                Spacer(modifier = Modifier.height(4.dp))
            }
            //для теста
            data class testClass(val name: String, val purpose: Int)
            val testList = listOf(
                testClass("Вычисления", 5),
                testClass("Эмоции", 3),
                testClass("Загадки", 1),
                testClass("Изучение действий", 0),
                testClass("Арифметика", 7),
                testClass("Фигуры", 0),
                testClass("Лиса Алиса", 4),
                testClass("Почемучка", 0))
            items(testList){
                Spacer(modifier = Modifier.height(4.dp))
                TestTextRow(it.name, it.purpose)
                Spacer(modifier = Modifier.height(4.dp))
            }
        }
    }
}