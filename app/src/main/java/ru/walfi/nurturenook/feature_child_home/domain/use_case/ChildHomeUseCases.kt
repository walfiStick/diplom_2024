package ru.walfi.nurturenook.feature_child_home.domain.use_case

data class ChildHomeUseCases(
    val childLogOutUseCase: ChildLogOutUseCase,
    val getChildIdUseCase: GetChildIdUseCase,
    val getTasksUseCase: GetTasksUseCase,
    val changeTaskStatusUseCase: ChangeTaskStatusUseCase,
    val getPurposesUseCase: GetPurposesUseCase
)
