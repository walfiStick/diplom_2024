package ru.walfi.nurturenook.feature_child_home.presentation.child_schedule

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_child_home.domain.use_case.ChildHomeUseCases
import javax.inject.Inject

@HiltViewModel
class ChildScheduleViewModel @Inject constructor(
    private val childHomeUseCases: ChildHomeUseCases
): ViewModel() {

    private val _state = mutableStateOf( ChildScheduleState() )
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var childId: Int? = null

    init{
        viewModelScope.launch {
            childId = childHomeUseCases.getChildIdUseCase()
            childHomeUseCases.getTasksUseCase(childId!!).onEach {
                _state.value = state.value.copy(tasksList = it)
            }.launchIn(viewModelScope)
        }
    }

    fun onEvent(event: ChildScheduleEvent){
        when(event){
            is ChildScheduleEvent.PressedLogOutButton -> {
                viewModelScope.launch{
                    childHomeUseCases.childLogOutUseCase()
                    _eventFlow.emit(UiEvent.LogOut)
                }
            }
            is ChildScheduleEvent.ChangedTaskStatus -> {
                viewModelScope.launch {
                    childHomeUseCases.changeTaskStatusUseCase(event.task)
                }
            }
        }
    }

    sealed class UiEvent{
        object LogOut: UiEvent()
    }
}