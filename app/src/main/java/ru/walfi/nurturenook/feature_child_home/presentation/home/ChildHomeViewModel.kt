package ru.walfi.nurturenook.feature_child_home.presentation.home

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChildHomeViewModel @Inject constructor(): ViewModel() {

    private val _state = mutableStateOf(ChildHomeState())
    val state = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: ChildHomeEvent){
        when(event){
            is ChildHomeEvent.ItemChange -> {
                if(_state.value.selectedItemIndex != event.index){
                    _state.value = state.value.copy(selectedItemIndex = event.index)
                    viewModelScope.launch {
                        _eventFlow.emit(UiEvent.OpenScreen(event.route))
                    }
                }
            }
        }
    }


    sealed class UiEvent{
        class OpenScreen(val route: String): UiEvent()
    }
}