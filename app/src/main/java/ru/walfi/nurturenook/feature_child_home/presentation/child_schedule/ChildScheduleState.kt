package ru.walfi.nurturenook.feature_child_home.presentation.child_schedule

import ru.walfi.nurturenook.core.model.Task

data class ChildScheduleState(
    val toolbarText: String = "Расписание",
    val tasksList: List<Task> = emptyList()
)
