package ru.walfi.nurturenook.feature_child_home.domain.use_case

import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository

class ChangeTaskStatusUseCase(
    private val repository: ChildHomeRepository
) {

    suspend operator fun invoke(task: Task){
        repository.updateTask(task)
    }
}