package ru.walfi.nurturenook.feature_child_home.domain.use_case

import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository

class GetChildIdUseCase(
    private val repository: ChildHomeRepository
) {

    suspend operator fun invoke(): Int{
        return repository.getChildId()
    }
}