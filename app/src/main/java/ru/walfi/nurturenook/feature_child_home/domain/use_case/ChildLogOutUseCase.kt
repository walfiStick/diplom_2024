package ru.walfi.nurturenook.feature_child_home.domain.use_case

import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository

class ChildLogOutUseCase(
    private val repository: ChildHomeRepository
) {

    suspend operator fun invoke(){
        repository.logOut()
    }
}