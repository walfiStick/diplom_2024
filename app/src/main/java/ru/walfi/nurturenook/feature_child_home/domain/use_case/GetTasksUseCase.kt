package ru.walfi.nurturenook.feature_child_home.domain.use_case

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository

class GetTasksUseCase(
    private val repository: ChildHomeRepository
) {

    operator fun invoke(childId: Int): Flow<List<Task>>{
        return repository.getTasksByChildId(childId)
    }
}