package ru.walfi.nurturenook.feature_child_home.presentation.home

sealed class ChildHomeEvent{

    class ItemChange(val index: Int, val route: String): ChildHomeEvent()
}
