package ru.walfi.nurturenook.feature_child_home.presentation.child_schedule

import ru.walfi.nurturenook.core.model.Task

sealed class ChildScheduleEvent{

    object PressedLogOutButton: ChildScheduleEvent()

    class ChangedTaskStatus(val task: Task): ChildScheduleEvent()
}
