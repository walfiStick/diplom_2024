package ru.walfi.nurturenook.feature_child_home.data.data_source

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.core.model.Task

@Dao
interface ChildHomeDao {

    @Query("SELECT * FROM task WHERE childId = :childId")
    fun getTasksByChildId(childId: Int): Flow<List<Task>>

    @Update
    suspend fun updateTask(task: Task)

    @Query("SELECT * FROM purpose WHERE childId = :childId")
    fun getPurposesByChildId(childId: Int): Flow<List<Purpose>>
}