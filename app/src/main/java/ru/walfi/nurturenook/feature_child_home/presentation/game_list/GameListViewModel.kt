package ru.walfi.nurturenook.feature_child_home.presentation.game_list

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.walfi.nurturenook.feature_child_home.domain.use_case.ChildHomeUseCases
import javax.inject.Inject

@HiltViewModel
class GameListViewModel @Inject constructor(
    private val childHomeUseCases: ChildHomeUseCases
): ViewModel() {

    private val _state = mutableStateOf(GameListState())
    val state = _state

    private var childId: Int? = null

    init {
        viewModelScope.launch {
            childId = childHomeUseCases.getChildIdUseCase()
            childHomeUseCases.getPurposesUseCase(childId!!).onEach {purposeList ->
                purposeList.forEach {
                    when(it.gameName){
                        "Изучение слов" ->{
                            _state.value = state.value.copy(learnWordPurpose = it.targetValue - it.completedValue)
                        }
                    }
                }
            }.launchIn(viewModelScope)
        }
    }
}