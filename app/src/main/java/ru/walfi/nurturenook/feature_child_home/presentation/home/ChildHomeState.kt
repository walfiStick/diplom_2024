package ru.walfi.nurturenook.feature_child_home.presentation.home

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.EventNote
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.SportsEsports
import androidx.compose.material.icons.outlined.AccountCircle
import androidx.compose.material.icons.outlined.EventNote
import androidx.compose.material.icons.outlined.List
import androidx.compose.material.icons.outlined.SportsEsports
import ru.walfi.nurturenook.core.util.BottomNavigationItem
import ru.walfi.nurturenook.core.util.Screen

data class ChildHomeState(
    val bottomNavigationItemsList: List<BottomNavigationItem> = listOf(
        BottomNavigationItem(
            title = "Игры",
            selectedIcon = Icons.Filled.SportsEsports,
            unselectedIcon = Icons.Outlined.SportsEsports,
            route = Screen.GameListScreen.route
        ),
        BottomNavigationItem(
            title = "Расписание",
            selectedIcon = Icons.Filled.EventNote,
            unselectedIcon = Icons.Outlined.EventNote,
            route = Screen.ChildScheduleScreen.route
        ),
    ),
    val selectedItemIndex: Int = 0
)
