package ru.walfi.nurturenook.feature_child_home.presentation.child_schedule.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.ui.theme.finishedTaskCardsColor
import ru.walfi.nurturenook.ui.theme.secondTextColor
import ru.walfi.nurturenook.ui.theme.textColor
import ru.walfi.nurturenook.ui.theme.unFinishedTaskCardsColor

@Composable
fun TaskCardForChild(
    task: Task,
    onCheckedChange: (Boolean) -> Unit,
    modifier: Modifier = Modifier
){
    Row(
        modifier = modifier
            .clip(shape = RoundedCornerShape(12.dp))
            .fillMaxWidth()
            .background(if (task.executeFlag) finishedTaskCardsColor else unFinishedTaskCardsColor)
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Checkbox(
            checked = task.executeFlag,
            onCheckedChange = onCheckedChange
        )
        Spacer(modifier = Modifier.width(30.dp))
        Text(
            text = task.description,
            style = TextStyle(
                color = if(task.executeFlag) secondTextColor else textColor
            )
        )
    }
}