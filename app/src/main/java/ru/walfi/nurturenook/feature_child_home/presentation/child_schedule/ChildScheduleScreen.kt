package ru.walfi.nurturenook.feature_child_home.presentation.child_schedule

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Logout
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collectLatest
import ru.walfi.nurturenook.core.util.Screen
import ru.walfi.nurturenook.feature_child_home.presentation.child_schedule.components.TaskCardForChild

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChildScheduleScreen(
    navController: NavController,
    viewModel: ChildScheduleViewModel = hiltViewModel()
) {
    val scaffoldState = rememberScaffoldState()
    val state = viewModel.state.value

    LaunchedEffect(key1 = true){
        viewModel.eventFlow.collectLatest { event ->
            when(event){
                is ChildScheduleViewModel.UiEvent.LogOut -> {
                    navController.navigate(Screen.AuthorizationScreen.route)
                }
            }
        }
    }

    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .systemBarsPadding(),
        scaffoldState = scaffoldState,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(text = state.toolbarText, fontSize = 18.sp) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Transparent
                ),
                navigationIcon = {
                    IconButton(onClick = { viewModel.onEvent(ChildScheduleEvent.PressedLogOutButton) }) {
                        Icon(
                            imageVector = Icons.Default.Logout,
                            contentDescription = "Выйти"
                        )
                    }
                }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 20.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ){
            items(state.tasksList){task ->
                Spacer(modifier = Modifier.height(4.dp))
                TaskCardForChild(
                    task = task,
                    onCheckedChange = {
                        viewModel.onEvent(ChildScheduleEvent.ChangedTaskStatus(
                                task.copy(executeFlag = it)
                            ))
                    }
                )
                Spacer(modifier = Modifier.height(4.dp))
            }
        }
    }
}