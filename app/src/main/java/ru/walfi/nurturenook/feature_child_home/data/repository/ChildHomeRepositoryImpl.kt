package ru.walfi.nurturenook.feature_child_home.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.walfi.nurturenook.core.data.data_source.DataStoreManager
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.core.model.Task
import ru.walfi.nurturenook.core.util.SavedAccountData
import ru.walfi.nurturenook.feature_child_home.data.data_source.ChildHomeDao
import ru.walfi.nurturenook.feature_child_home.domain.repository.ChildHomeRepository

class ChildHomeRepositoryImpl(
    private val dataStoreManager: DataStoreManager,
    private val dao: ChildHomeDao
): ChildHomeRepository {

    override suspend fun logOut() {
        dataStoreManager.saveAccountData(
            SavedAccountData(
                role = "",
                accountId = -1
            )
        )
    }

    override fun getTasksByChildId(childId: Int): Flow<List<Task>> {
        return dao.getTasksByChildId(childId)
    }

    override suspend fun updateTask(task: Task) {
        dao.updateTask(task)
    }

    override suspend fun getChildId(): Int {
        return dataStoreManager.getAccountData().first().accountId
    }

    override fun getPurposesByChildId(childId: Int): Flow<List<Purpose>> {
        return dao.getPurposesByChildId(childId)
    }
}