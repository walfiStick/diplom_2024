package ru.walfi.nurturenook.feature_child_home.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.walfi.nurturenook.core.model.Purpose
import ru.walfi.nurturenook.core.model.Task

interface ChildHomeRepository {

    suspend fun logOut()

    fun getTasksByChildId(childId: Int): Flow<List<Task>>

    suspend fun updateTask(task: Task)

    suspend fun getChildId(): Int

    fun getPurposesByChildId(childId: Int): Flow<List<Purpose>>
}