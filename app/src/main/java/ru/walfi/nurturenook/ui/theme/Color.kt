package ru.walfi.nurturenook.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

//main color
val whiteColor = Color(255, 255, 255)

val buttonColor = Color(97, 117, 255)
val secondButtonColor = Color(186, 247, 211)
val backgroundColor = Color(217, 217, 217)

//textfield color
val borderTextFieldColor = Color(122, 123, 133)
val hintTextFieldColor = Color(112, 112, 112)
val contentTextFieldColor = Color(95, 136, 111)

//radiobutton color
val selectedRadioButtonColor = Color(1, 59, 23)
val unselectedRadioButtonColor = Color(144, 153, 140)

//loading animation
val loadingAnimationColor = Color(0, 117, 13)

//text color
val textColor = Color(10, 10, 10)
val secondTextColor = Color(122, 122, 122)

val childCardsColor = Color(187, 224, 191)
val commentCardsColor = Color(245, 245, 245)
val finishedTaskCardsColor = Color(235, 255, 237)
val unFinishedTaskCardsColor = Color(245, 245, 245)

val backBasicTextColor = Color(243,243,243)